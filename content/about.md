---
title: About
date: 2023-12-04
---

{{<figure
  src=/karl.jpg
  alt="Grayscale headshot of Karl with a blank expression."
>}}

Hi, I'm Karl, an embedded software developer living in Lancaster, Pennsylvania.

Cultured in Linux and holding a B.S. degree in electrical engineering, I've worked on technology for eight years.
I focus on bettering embedded systems by taking inspiration from the wider world of software engineering.
You might not find a person more zealous than me about code quality.
I believe better code and better English require clearer thinking.

Outside of work, I savor synthesizers, my family, and the humanness of bicycles.

To stay updated with my work, you can subscribe to this blog's [RSS feed](/index.xml).

This site has no ads or tracking. If you want to support my work, I'd love if you could [donate](https://liberapay.com/kdsch/).

## Contact

Email: <k@kdsch.org> \
Code: <https://git.sr.ht/~kdsch> \
Fediverse: [@kdsch@fosstodon.org](https://fosstodon.org/@kdsch)
