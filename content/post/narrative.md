---
title: Free-flowing software stories
date: 2023-05-19T04:42:37-04:00
---

There's an argument that despite the prevalence of search engines,
Wikipedia, and large language models (LLMs), we still need to [know
things][know]. We shouldn't just look things up.

I think this argument could use better examples. Canonical knowledge,
like Newtonian physics and text encodings, is not the best example of
something you should memorize. Sure, you'd be faster at solving a problem
if you know something relevant to that problem. But in some contexts,
predicting what problem you'll have, and what knowledge you will need,
is hard. Knowing _how_ to look up information is nearly as effective
as knowing _that_ a million facts are true, and doesn't require me to
adopt the habit of [spaced repetition][].

[know]: https://controlaltbackspace.org/know/
[spaced repetition]: https://controlaltbackspace.org/repeat/

## Spaced out

I used spaced repetition software (SRS) for five months in 2022. I was
looking for a job and not feeling so good. I hoped my job interviews
would go better if I had memorized a certain set of facts. I wrote 617
Anki cards, used the app every day, and savored the challenge of writing
good cards. I gave up before I got a job.

What actually helped my interviews was focusing on my impact. The real
problem was that I didn't feel confident. I didn't feel confident because
I was telling the story of my previous work in terms of activities
rather than impact. I hadn't made sense of what I'd done before. I
didn't understand what my work meant to hiring managers. In their jargon,
I didn't understand my differentiators.

To me, a differentiator is a circuit element, not an attribute of a job
candidate. But I've read some Gregory Bateson. I remember his motto:

> Information is a difference that makes a difference.

Daniel Dennett has recently examined the idea [too][edge].

[edge]: https://www.edge.org/conversation/daniel_c_dennett-a-difference-that-makes-a-difference
## Dennett does differentiate

Dennett used to think you could reduce meaning to propositions, but
he changed his mind. He now argues that semantic information generally
isn't propositional. Propositions sometimes usefully approximate
meaning. But if you want to get semantic information completely,
you can't assume a proposition will do the job.

In chapter six of of his book _From Bacteria to Bach and Back_, he connects
semantic information to design, and design to research and development
(R&D).

> So let’s consider, as a tentative proposal, defining semantic
> information as _design worth getting_, and let’s leave the term
> “design” as noncommittal as possible for the time being, allowing only
> for the point I stressed in chapter 3, that design without a designer
> (in the sense of an intelligent designer) is a real and important
> category. Design always involves R&D work of one sort or another, and now
> we can say what kind of work it is: using available _semantic information_
> to improve the prospects of something _by adjusting its parts in some
> appropriate way_. (An organism can improve its prospects by acquiring
> energy or materials—food, medicine, a new shell—but these are not
> design improvements. Rather, they are just instances of refueling or
> rebuilding one’s existing design.) One can actually improve one’s
> design as an agent in the world by just learning useful facts (about
> where the fishing is good, who your friends are).


## I don't get it

The four-strong embedded software team at my company is having a tough
time. We maintain a lot of software we didn't write. We all joined the
company within the last three years. Our software is complex. We have
the artifacts, but the artifacts are not the same thing as the design
(in Dennett's sense) of the people who created it.  It's hard to for us
to improve our design without the original authors around to mentor us.

Our software is not canonical like Newtonian physics.  It's
esoteric. There's public information about it, but it hasn't benefitted
from centuries of cultural evolution. It isn't private thoughts[^1],
but also isn't widely understood.

[^1]: Sometimes I get the urge to query the web for the location of an
object in my house.

I've thought of my job as maintaining, modernizing, or reverse engineering
old software. But that understanding has continuously underestimated the
difficulty of the job. It keeps getting harder.

Is it software archaeology?

Actual archaeologists use the term "software archaeology" as a
[joke]. Meanwhile, Embarcadero has made it into a Java [solution]! Software
archaeology certainly intersects with reverse engineering, but focuses
more on the social context ("Why is it designed like this?") than the
technical artifact ("What is its design?").

Sometimes software archaeology helps reverse engineering. When it is difficult
to glean the design from the artifact, the social context can provide critical
clues.

[joke]: https://archaeoinformatics.net/software-archaeology/
[solution]: https://www.embarcadero.com/solutions/software-archeology


## Regime change

Organizations grow regimens of information. A document in Confluence
always has an author, a set of viewers, a set of tags, a comment
thread. Teams to chase down what matters with mental models like OKRs.
Architecture decision records (ADRs) are hoped to ease archaeology for
future people. But I'm astonished at how [belabored] an ADR is, especially
the "status" section. How can you label a decision as "accepted"?  _Who_
accepted it? Who didn't? The ADR in which ADRs are "accepted" is _not_ an
architectural decision about software!

[belabored]: https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions

Acronym-laden formats, like propositions, have tradeoffs.  A rigid data
format is consistent and predictable, but sometimes infeasible. Things
that matter don't always fit into a consistent format. Just because you
have a consistent format doesn't mean your readers are free from the
burden of interpretation.

People need to learn things, but each person learns for themselves. Learning
isn't the accumulation of information; it is the redesigning of an organism.
Knowledge isn't _transferred_, it's only _developed_.

I want to encourage the development of knowledge. My current
hypothesis? As far as _formats_ for semantic information go, **stories**
and **dialogues** are probably the best you can get. Smullyan's [Is God
a Taoist?][taoist], my favorite dialogue, doesn't follow "bottom line
up front" (BLUF) or the [Minto pyramid]. The conclusion is at the end,
after inquiry has dug up what matters. Maybe information optimized for
fast consumption isn't a good fit for archaeology.

[taoist]: https://www.mit.edu/people/dpolicar/writing/prose/text/godTaoist.html
[Minto pyramid]: https://untools.co/minto-pyramid


## Try it

When somebody has a story worth telling, let them share it. The medium
doesn't matter. Pick something convenient for your situation. Don't
enforce a rigid schema. Encourage semantic information.

When I worked with the Auklet team, I told stories through Jira work
logs. My boss loved it and my coworkers found useful info in my work logs.

At my current company, I've put a `stories/` directory in the main
project repository. This is inspired by the idea of keeping documentation
in the repository. Maybe a story is social documentation. In software
archaeology, social documentation makes a difference.

