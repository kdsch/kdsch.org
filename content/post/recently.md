---
title: "Recently"
date: 2021-11-12T04:55:10-05:00
---

## Reading

I read Ursula K. Le Guin's [_The Dispossessed_][leguin] to great
inspiration last month. It's the first novel I've read since _The Kite
Runner_ when I began college. The first paragraphs took a bit of
acclimation, but thereafter I was hooked.

[leguin]: https://libcom.org/library/dispossessed-ursula-le-guin

I want my child(ren) to read it, maybe after some education in philosophy
and science. The book's effect isn't to predict future technology,
but to affirm the complexity of humanity and to challenge us to meet
ourselves with a philosophy that deserves us. For Le Guin, [Taoism][]
is such a philosophy.

[Taoism]: https://plato.stanford.edu/entries/daoism/

> To learn which questions are unanswerable, and not to answer them:
this skill is most needful in times of stress and darkness.

Deb Nicholson published a fascinating piece, [A Club is Not a
Movement][twc], in the Tech Workers Coalition Newsletter.

[twc]: https://news.techworkerscoalition.org/2021/10/26/issue-22/

Forget voting with your dollar. Markets aren't going to protect
us from climate change. [Unionize][]!

[Unionize]: https://www.jacobinmag.com/2021/11/climate-change-market-based-solutions-cop26-grassroots-organizing-power


## Happening

We feel in limbo as my son is old enough to be in daycare, and even to
benefit from the socialization, but a staffing shortage keeps pushing
back his start date. We feel ambivalent about him going to daycare given
Covid's unpredictability. His older cousin has gone for a long time and
tends to catch a lot of bugs.

I'm looking for full-time work, as we feel that might strike a better
balance than my wife working full-time.


## Listening

A recent [episode][] of _The Dig_ has improved my life.

[episode]: https://www.thedigradio.com/podcast/the-right-to-sex-with-amia-srinivasan/

It's currently raining.
