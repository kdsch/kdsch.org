---
title: PCB edge cuts from a 3D model
date: 2025-02-13
---

I'd like to explain how I use FreeCAD to create PCB edge cuts in KiCad.

I am starting from a STEP model of an off-the-shelf cast aluminum
enclosure sold by Hammond. These are very popular for guitar pedals and
other equipment. After selecting an enclosure that I think is the right
size, I want to create a PCB edge cut by grabbing some of the geometry
from the STEP model and extracting it into a SVG file. This creates
the largest PCB that will fit in the enclosure. KiCad will let you
import an SVG directly into the PCB edge cuts layer. And in Inkscape,
the edge cuts SVG can be useful for understanding the 2D constraints of
the design.

I was unsure of the correct way to do this. And I think I'm still a
little unclear, because FreeCAD is a pretty sophisticated tool. But the
approach that's working for me right now is this:

1. In FreeCAD, open the STEP file of the enclosure.
2. Hit **V, 3** for wireframe view, which makes it easier to select edges.
3. Control-click each of the edges you want.
4. Click the green **Shape Binder** button.
5. In the **Data Tab**, change the angle and axis of the Shape Binder
   so that it is parallel to the XY plane. Check by hitting **2** for
   the top view.
6. Make sure the Shape Binder is selected.
7. In the **View Tab**, under Display Options, make sure **Display Mode** is
   set to "Wireframe". KiCad respects fills, and we don't want the edge cut to
   have a fill.
7. Hit **Control-E** to **Export** and choose the flattened SVG file type. (I
   think "flattened" means projected onto the XY plane, which is why we had
   to rotate the object.)
8. In KiCad's PCB Editor, hit **Control-Shift-F** to import graphics.
9. Choose the file you exported from FreeCAD.
10. Choose the Edge Cuts layer.

A limitation of this approach is that the SVG is all straight lines;
there are no arcs. This seems to be a consequence of directly using
the Shape Binder, which copies the objects from the STEP file. You're
stuck with whatever the STEP file uses, as far as I can tell.

Some PCB designs will need customized edge cuts. There is a way to edit
the SVG before importing to KiCad. In FreeCAD, after creating the Shape
Binder, you can create a **Sketch** and extract vertices or edges from the
Shape Binder to use as constraints or reference points for a customized
edge cut design. For example, if your design has two PCBs with a ribbon
cable between them, you could use a Sketch to create a board based on
the geometry of the enclosure but with a cutout for the ribbon cable.

