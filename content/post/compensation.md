---
title: "\"Compensation\""
date: 2021-11-28T21:07:31-05:00
---

December 2019, Lancaster, Pennsylvania. CareerLink office. The
tone was set by the front desk staff. I was asked what I was here
for. "R-E-S-E-A." "OK, re-see ya." As I signed in, I was told to check
a certain box, but I did not see which one. I guessed correctly.  I was
then directed down the hall to the third door on the right, a small
classroom.  I had to sign in there, too. Did I have my letter? Yes. Did
I have my 304's?  I don't know what that is. "It's OK; I'll explain it."

I sat down at the back of the room next to a black woman. A man
delivered a PowerPoint presentation, first explaining his background:
recently unemployed himself for five months and a long career in the
military. He spoke with a clear and even timbre, neither optimistic nor
pessimistic. It could have been the sound of desk rule. The sound of
the clash between official rules and popular feelings: conciliatory,
equivocating, and firm. By which attendees' anticipated confusion is
resolved by shielding officials from liability.

After the presenter detailed the services available to us, we were
enqueued in the order in which we signed in for personalized interviews
with the staff. It's a good thing I wasn't among the first four to sign
in, because I would have been unprepared.

Somehow I had missed the requirement to bring the work search
records. It's right there in the letter. No worries; I'll have Steph send
photos. I copied my 304's as I waited. I listened to women hushing their
stories as the men locked themselves away. I saw people of all ages,
and most of them had a distinctly lower-class ethos. In my imagination,
it would have been the jagged, drooping face of a lifelong smoker;
the blemishes of abuse, neglect, and hardship; the tawdryward attempt
to appear above one's means. When my deskmate was called, she wished me
good luck. U2, I reflexively reciprocated.

I sensed potential. People united by difficult circumstances, brought
together in a room by fate. But in the grammar of this experience, we
would not be of any consequence to each other. Our function in society
would still be decided by others. This is not a time for creativity,
but solemn compliance.

I was called, and before I had gathered my belongings, my interviewer
was already back at her cubicle. I weaved through open floorplan and
sat down on the outside of her desk, the back of which was walled
off with beige-grey sheet metal, meant to be against a wall that did
not exist. I was the wall.

"I'll need your paperwork, please." I'd been given plenty and I wasn't
sure what all she was referring to. "Such as?" I opened my folder for
her to see.

She took what she needed, and that was the end of the typical bureaucratic
bluntness. She commenced a monologue on attendees' misunderstanding
of the organization's purpose and her workstation software's absurdity.
Squeezing what of my wit remained, I joked, "On behalf of all programmers,
I apologize."

I was told I hadn't completed my 304's according to requirements but that,
by her judgment, I had shown good faith. She warned that I would "definitely
receive a scary do-better letter". But my benefits would not be revoked.

She printed off a bunch of pages for me to take home. "Do you have any
questions?" "No," I emitted reflexively. I should have asked if she had any
answers. She reached out for a handshake, finishing off the interview
with, "Well, have a great day!"

I hunched home in the acrid breeze.

