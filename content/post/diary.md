---
title: A decade of diarism
date: 2023-12-11T12:04:53-05:00
---

Ten years ago, I wrote the first post in my personal, electronic diary. It
actually wasn't my first try with diarism, but it's the one that survived.

I wasn't spurred to adopt it by a faddish impulse to improve myself. I
realized that I got something from self-reflection. That realization began
in a tender time of burdensome belief and fallacious faith. Somehow,
despite my half-closed mind, I couldn't suppress my desire to think, to
understand myself.

I remember dimly one of the short-lived attempts. It was `spirit log.txt`,
authored in Notepad. I bottom-posted and used F5 to datestamp each entry.
The scope quickly expanded to include any of my thoughts, not just the
spiritual ones. The rationale was that if I am a spiritual being, all
of my thoughts are spiritual.

The current diary is `log.md`, edited with `vis`. I still use F5 to
insert a datestamp, via a custom keybinding. An odd legacy from my
era of frustration with Windows.

Despite my attempts to write more on this website, `log.md` receives
most of my attention. The file has grown to 5.2 megabytes. I believe the
longevity of the habit stems from ease in creating new entries. There
are no titles, no new file paths to specify. I type `vis log.md`, zip
to end-of-file with `G`, hit F5, and write. There is no maintenance
overhead other than backing up when I worry about losing it.

A few weeks ago, I wrote a script to parse the Markdown into a date-based
file tree. I even added Git tracking with retroactive commit dates
scraped from the entry headers. But I couldn't devise a low-friction
way to create new entries. I kept using my old, one-file approach. The
habit kept working.

The diary, or log, as I call it, has all kinds of stuff. I can
read how I changed. I captured a mathematical, philosophical period.
Some writings were polished enough to publish. Most have never been
edited.

That's something I learned recently, that editing makes good writing.
It's just like code. If you don't maintain it, it rots. But of course
I already knew that. I remember fondly editing English papers in high
school. My enjoyment of English has persisted over two decades.

Feeling joy for English is apparently an unusual trait for engineers,
as engineering is an unusual trait for English writers. I'm used to
being unusual, and maybe I can benefit from that.

Profit? I've never had confidence in my ability to sell what I write.
It doesn't seem like the right approach for me.

The appreciators of my writing are scattered in the nooks and crannies
of my life, like mothballs. I remember when my nurse practitioner asked,
through the online messaging portal, "Do you write?" I guess it's less odd
to get a thumbs up from my boss for writing nice [work logs] in
Jira. Still, these are not genres that most people put a lot of effort
into.

[work logs]: http://localhost:1313/2020/02/17/useful-work-logging/

I guess that's evidence that my care for language is genuine and intrinsic.

I like Brad Taunt's encouragement to write [brain dumps]. That feels like
the right advice. But the terminology is disgusting.

[brain dumps]: https://bt.ht/dump/

Intellectual defecation isn't much better.

Wait, there's another literary engineer. Who was it? It was the guy Rob
Pike quoted in his talk, "[Public Static Void]". Dick Gabriel!

[Public Static Void]: https://youtu.be/5kj5ApnhPAE?feature=shared&t=28

[Richard P. Gabriel](https://dreamsongs.com/), to be precise.

Surely, he would agree that we can do better than to call these writings
brain dumps. You know, this also connects with [Zinsser]'s idea that
good writing is warm and human, that it evokes the writer's enjoyment.

[Zinsser]: https://en.wikipedia.org/wiki/William_Zinsser

So, I don't have any suggestions, but I'm not going to call this a
brain dump. That sounds too much like engineering speak.


