---
title: Promoting a culture of mental health
date: 2022-05-30T20:51:32-04:00
---

America has a culture problem: we don't know how to promote mental health.
While I view mental health---and public health generally---as a collective
responsibility, sharing what you've learned about mental health nudges
the system in the right direction. So that's what I'm going to do in
this post.

---

My life was never as stressful as during the coronavirus pandemic. While
much media coverage about workers during this time told the story of
their awkward transition to working from home, my situation was totally
different. I had been laid off (you know---fired, but politely) from my
fully remote job a few months prior and was in the middle of a delicate
career transition. I had just begun a new professional networking effort
and had made some good connections; I even did some consulting work for
one of them.

Then my first child was born, right at the crest of the first wave. He
suffered from some health complications. Because of the pandemic, my wife
and I had less support from extended family than we expected. From sleep
deprivation and the constant burden of care, I was burnt out and angry. I
felt that life had gone out of control. I had a nervous breakdown. I
was terrified, and decided to get a therapist.

His name was J. He took an introspective approach with me, searching
for previous experiences that could explain my undesired responses
to emotions. Though I was disappointed with this approach, I stuck
with it for a few months because I didn't know what else to do. But
eventually, I seemed to get so little benefit that I decided to discontinue
services.

Some time later, another difficult episode brought me to action again. I
went back to the same organization, and to my surprise, J. hadn't
discharged me.  My file was active, a bureaucratic good fortune, and I
didn't have to wait in a queue to receive services. (Thanks, J.)

I felt J. and I didn't work well together, so I requested a different
therapist. My new therapist, S., took a practical, undogmatic
approach with me. Our sessions were often under 30 minutes and very
pithy. She didn't explain my behavior in terms of past life events.
Rather, she saw it as a lack of skill in awareness ("mindfulness"),
coping, and communication. With my situation framed this way, I could
improve my behavior by learning specific skills.

What's more important: postulating untestable hypotheses about your
behavior, or learning how to behave differently?

During each session, I usually wrote some notes, such as questions to
ask my wife and techniques to try. While my therapist provided me with a
large body of [materials][dbt] for self-education, she never drilled me
on it. I explored it at my own pace and identified the parts that spoke
to me. That gave me a sense of ownership of my situation, and also made
me feel great respect and comfort with her. She trusted my ability to
evaluate information and come to a sound conclusion.

[dbt]: https://dialecticalbehaviortherapy.com/

During a time when I felt deeply ashamed of my undesired behaviors, which
had damaged my relationships, I benefited tremendously from S.'s
faith in me. This faith, coupled with her pragmatic approach, gave me
enough support to make progress. She sought the good and the useful in
me, so that I could expand those qualities.

I felt surprised when she referred to my progress. I felt doubt; it
was unclear to me what factor was limiting me: my lack of skill, or my
environment. She admitted that sometimes environment _is_ the limiting
factor. I reasoned, if I'm improving, and it's because my environment is
becoming less stressful, then I haven't necessarily learned anything. I
didn't want to delude myself about what was happening to me. Despite
my doubt, S. consistently referred to my progress and encouraged me to
recognize it. This validation improved my self-confidence and helped me
to persevere when other people in my life didn't have as much faith in me.

Today, I still have most of the same feelings I've always had---life is
still stressful, lonely, and bleak---but I often respond to my feelings
differently than I used to.

One of the first techniques I learned is to simply remove myself from
excessively stressful situations. It's 5:36pm, I'm tired, and my son is
fussy at the dinner table? Finish my plate and clean up the kitchen. I
had to learn to recognize when this is the right technique.

Sometimes doing the right thing for yourself changes based on your skill
level. When I began this journey, I had pretty low stress tolerance,
so I used avoidance often. My wife didn't always understand or approve
of my avoidance, but in retrospect it was a way for me to prevent an
escalation that I lacked the skill or energy to handle. Also, I didn't
use this technique to gain approval, but to survive. Sometimes you have
to say, "Haters gonna hate."

A large component of mental health is social dynamics. As my journey
continued, the most frequent subject in my sessions shifted from stress
management techniques to communication and my relationships. Coping with
other people's non-assertive communication styles is an important skill.
When you start trying to take care of yourself, other people might have a
problem with it. If those people are going to remain part of your support
system, they inevitably have to learn some mental health skills, too.
Fortunately, in my case, my wife was also in therapy and takes her share
of responsibility for our dynamics.

I think we live in a nation of people who are uneducated, untrained, about
many skills that are required to maintain mental health. Or worse, they've
learned harmful techniques through toxic culture. People are coping with
their feelings in ways that harm themselves, their friends and family,
and the public. This isn't the only problem in the country with respect
to mental health, but it's a problem I can relate to because I've been
there. And most people in my family are passive communicators who tend
to stew. But I've observed other families where aggressive communication
pervades, and they tend to have more verbal and physical violence.

[Assertive][assert] communication is healthy and simple: say what you
think, feel, and want. Promoting it is equally simple: ask what others
think, feel, and want. This is easily the most powerful skill I've
learned so far, and I'm still learning all the ways it can be used.

[assert]: https://dialecticalbehaviortherapy.com/interpersonal-effectiveness/communication-styles/

Here's an example of something I said using this communication style:

> Sometimes I don't apologize to you because **I feel** afraid of being
rejected. **I want** my sincere apologies to be recognized. When they
aren't, **I think** there's no point to apologizing.

What's so good about this? It first focuses you on recognizing and
accepting your own thoughts, feelings and desires. Self-invalidation
is a serious obstacle to mental health. And by making statements about
yourself, you aren't making accusations. You're not saying anything
harmful, only making statements about yourself, which are entirely within
your rights. By communicating this way, you frame the conversation in
much more productive terms; instead of trying to assign blame, it becomes
about accepting the social facts of life. This allows much more effective
problem solving. Consider this destructive framing:

> You always reject my apologies, so I have no reason to apologize anymore.
I suppose you prefer my apologies insincere, is that more useful to you?

These are the words of a hurting person who isn't in the habit of talking
about their feelings. Instead, they make their feelings about qualities of
other people, like them being irrational. This results in arguments about
what qualities people really exhibit, which is a dead-end. This kind of
communication, if not remedied, destroys relationships.

```


```

Not all therapists are alike. You need an ally: a person who believes
in you and thinks you have the basic ability to succeed. Ideally, it's a
person you can relate to; who you like enough that you'd be friends. Avoid
spending time with somebody who thinks less of you, who misunderstands
you, who repeatedly fails to deliver the results you need. The therapist
should provide real-time expertise to help you troubleshoot a system you
don't deeply understand. They're not troubleshooting, they're providing
domain knowledge. And they're teaching you how to fix breakage you
identify.

You don't necessarily need a therapist, but if you find yourself in a
cycle of harm that you don't know how to stop, you don't have much to
lose by trying to find one.
