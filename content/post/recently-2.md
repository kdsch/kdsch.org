---
title: Recently
date: 2024-10-31
---

My life hasn't turned out as I expected it. Being a husband a dad and a
homeowner and a professional software engineer is harder than I thought
it would be. And luck has not always served my health. I had a pituitary
tumor removed last year --- the surgery went fine, although recovery was
rough. We learned the hard way that my pituitary gland has no function (I
almost died from a stomach virus), so I take hormones. I lost my libido
and realized that it was probably from a combination of testosterone
and oxytocin, two chemicals that my body does not produce. I welcomed this
insight as a scientific explanation of human behavior.

It's been a time of new explanations all around, mysteries upgraded. When
my son turned three, he was diagnosed with autism. His mother and I
welcomed it --- we sought it. We took it as a chance
to outdo our parents, show them what it means to be sensitive to a
child's needs.

Otto adores us. But it takes energy to be attentive and supportive and
not lash out. We are constantly fighting for more energy. We couldn't
understand why.

My mother, from whom I'm estranged, sent me a father's day card that
contained a letter she wrote to my first grade teacher. The teacher had
asked all the parents to write letters describing their children. My
mother's letter, written in the early 1990's, described a child quite
like my son. BANG, a surge of neuron firings, and suddenly my brain was
different. I realized am autistic and I passed it on to him genetically.

Autism explains everything: my difficulties with working for money, my
trouble making friends, my obsessive and deep interests, my sensitivity
to noise, my bluntness and concreteness, my repeated difficulties with
schooling. Decades of shame vanished as I realized my "anger problem"
was a natural autistic reaction to overstimulation.

I sit here now having quit my job and having no authentic interest in
working or writing code, and _absolutely no ability to feign_. Something
I used to deeply enjoy has become a symbol of toxicity and ableism. I
don't know who I am anymore. I take life one day at a time. I believe
I'm recovering from burnout and that eventually I will be able to work
again, but I have no idea when that will happen. And this recovery mustn't
be rushed because it is a recovery from a lifetime of constant burnout that
was pushed beyond the limit by becoming a working father.

I'll see you on the other side.
