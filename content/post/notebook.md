---
title: Writing system
date: 2024-07-22
---

I used to grab printer paper and scribble on it when I wanted to write something down.
I'd often use the letter-sized sheets in landscape orientation to better fit between my keyboard and the edge of my desk.
This led to stacks of loose paper piling up in my office.
When I felt the need to declutter, I had to painstakingly decide page-by-page what to keep and discard.
About two years ago, I got tired of this system and began to think about improvements.

My problem-solving process was straightforward. I knew why I was using loose paper:

1. Writing on paper helps me think
2. Printer paper is convenient
3. Landscape orientation makes it more compact
4. Loose sheets allow a hard backing surface

And I had to fix the problems:

1. Disorganization
2. Difficulty deciding what pages to keep and discard
3. Graphite smudging

I had tried various notebooks before, but was displeased:

1. Staple bindings don't lie flat, causing messy writing
2. Rough paper, poor writing experience
3. Field Notes are trendy but too small
4. Subject-specific notebooks made me question the aptness of my topic

Therefore my criteria were:

1. Spiral binding
2. Smaller than letter, bigger than pocket
3. All subjects welcome
4. Smooth paper
5. Smooth, low-smudge pen

I chose

- Maruman Mnemosyne N104, a B5-sized, 80-sheet spiral-bound notebook with 5-mm dot grid paper
- Uni-Ball Signo RT1 0.28mm, a fine-point gel pen
- [Architectural letterforms](https://jaredgorski.org/writing/penmanship/), an all-caps style based on repeatable strokes. Includes slashed zero

I enjoy this system. I spend no time deciding what to keep and discard --- I keep everything. All of my writings are in one place. Everything is readable -- my handwriting has been noticed as "like a typewriter". The dot grid is versatile, supporting mathematical plots, prose, and checkboxes.

It's OK to be a person who puts this much thought into your writing system if you enjoy it.
