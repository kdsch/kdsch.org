---
title: Parallel development
date: 2024-02-07T15:52:31-05:00
---

Sometimes when I am developing something, I want to do an experiment
or implement a prototype which is based on an existing design. I've
found that modifying an old design to accommodate a new idea can be
difficult. Also, it tends to make it hard to compare the new version
with the old version, which is a really useful thing.

## Refactoring a function

For example, when refactoring a function, I think it's better to write
a new function. This allows you to test --- or better, _prove_ --- that
the two versions agree. If you're refactoring a function that your code
calls directly in many places, this approach allows you to introduce the
new function gradually rather than everywhere all at once. If you've
ever faced lots of compiler errors during refactoring, you know what
I'm talking about.

## Migrating to a new platform

I've reached a point in one of my projects where I think a real-time
operating system (RTOS) would be useful. The program will have some
mostly independent concurrent tasks, and it would be easier to let the
scheduler ... schedule ... them, rather than me dictate how to break
up the tasks and order them in time.

I'll probably go with [FreeRTOS]! But adding it to the existing MCU
application has been tough.

The parallel approach can help. I can add a separate target to my build
system which will allow me to bring up a stub FreeRTOS system. After I've
got FreeRTOS working on the microcontroller, I can migrate the application
to the FreeRTOS system. Having two build targets will allow me to build
the old and new executables simultaneously and easily compare them.

[FreeRTOS]: https://www.freertos.org/

## Trying a new graphic design

I recently designed some business cards for myself, to hand to people
I see often but haven't given my contact info yet.

After I got the prints, I noticed certain aspects of the design that
could be improved. I could have used a smaller, sans-serif font. I
could have drawn a circle with a thicker stroke and a cool color.

I changed the SVG file, and now have a design that might be better.
Is it actually better? The old no longer exists. The parallel approach
would make it easier to compare the two designs.

## Version contol can be parallel

Well, I lied. I versioned my SVG file using Git, so I still have the old
design. Moreover, I don't have to manually check out the old version
and copy it to a separate file. Git supports multiple worktrees, so I
can do something like

	git worktree add previous/ HEAD^

This will create a new directory `previous/` that contains the original
design.  When I'm done comparing, I can remove the worktree with

	git worktree remove previous/

But this isn't the best approach. I could have added the new design
as a separate layer in the SVG file, allowing immediate comparison.

## Insert your situation here

How might you apply the parallel approach?

