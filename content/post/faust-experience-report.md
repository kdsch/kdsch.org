---
title: "Faust: an experience report"
date: 2022-02-16T20:51:17-05:00
---

For those who do not know, Faust is a domain-specific language (DSL) for
developing audio digital signal processors. It is a functional programming
language developed by [GRAME][], a French research institution, since
about 2002. The Faust compiler, and the majority of accessory code,
is written in C++, although the compiler also targets other languages.

[GRAME]: https://grame.fr

I've been using Faust for about two years. I found it a pretty easy
language to learn, given that I was already comfortable with functional
programming. The five block diagram operators still seem a bit magical,
but I get by.

I have enjoyed writing DSP code in Faust because of the straightforward
syntax. A main reason anybody wants to use a DSL is to focus on a
particular aspect of their software. In the case of audio processors,
signal processing is the main aspect, and Faust does a brilliant job of
bringing it into focus.

Of course, audio processors don't just process signals; they also deal
with their environment through audio, user, and system interfaces. Faust
doesn't directly handle these aspects, although it has...  _ideas_
about them.

The Faust project is marketed to who might be called "music hackers";
musicians and/or programmers who write DSP but not environment-interfacing
code. (I guess "DSP engineer" doesn't sound cool enough.) Faust brings
these users value by providing not just a DSP language compiler, but
also high-quality libraries authored by people who know their stuff,
tools that make development accessible and interactive, and a wide range
of target platforms supported through scripts. Faust is a playground
for music hackers.

I am both a DSP engineer and a system programmer, so it was only a matter
of time before I began to work with Faust seriously.  I'm also an amateur
programming language designer who has worked on functional programming
languages, so Faust offers me many things to reflect on. But before we get
to all of that, I want to discuss what Faust calls an "architecture file".

I find the term "architecture" unfortunate. I understand why the term
was adopted; it's the conceptual negation of DSP, and for Faust, →
DSP ← is the center of the universe. But not all potential Faust
users care only about DSP. Some of them also work with audio, user,
and system interfaces, and the term "architecture" does not serve them.

DSP is simple: nothing but a transformation applied to a data stream. It
need have no concept of anything more than floating-point arithmetic. This
is the part of Faust I find most sound and useful. It truly fulfills
its role as a DSL; it distills the essence of signal processing problems
into a small set of primitives.

The music hacker playground is not well anchored to this elegant core.

Faust is not, and has never been, a pure signal processing DSL. From
the beginning, it has always provided special syntax for _GUI_
primitives. Yes, the "DSP" language has ideas about _user interfaces_,
because it is a language for music hackers, not for DSP engineers.

But of course, the Faust compiler doesn't own these GUI primitives. It
doesn't tie you, the music hacker, to Qt or JavaScript or GTK or Cocoa. It
merely translates them into calls that the architecture has to implement.
You pick the architecture, it'll handle the GUI libraries for you.

I understand. There can't be a playground without supporting a wide
range of platforms.  And as far as I can tell, Faust has passed up ways
to attract contributors who work at this level. There isn't enough
documentation on writing architecture files, on the interfaces and
contracts that the Faust compiler delegates to architectures.

This seems to assume that all architectures will be maintained under the
Faust project, rather than viewing architecture authors as users. Perhaps
earlier, the project expected to encapsulate the architecture-DSP
interface.

It was not to be. Faust architectures are not equal in their support of
primitives.

Another way that Faust was originally marketed was as a language for
writing plugins. I think that's fitting; Faust isn't really a DSP language
but an audio plugin language that happens to have a really cool functional
DSP core.

I like the core, I don't think the rest of it works for my situation. And
I think this is a case of being too specialized. There's no reason Faust
has to make my use case hard; in principle, it's possible to make a pure
DSP language that can be optionally extended for writing plugins. I don't
see why both use cases can't be accommodated.


I'm one of the users that finds myself on both sides, and I still
think Faust's core syntax provides value. The problem I face is the
empty pretense of a UI contract, assumptions about what kind of memory
allocation I will use and about how I want to integrate the compiled
DSP with my project. This layer has been neglected, and users at this
level have been ignored.

I have nothing against building the music hacker's playground. I just
want it to be built out of parts that are also useful to me.


There are no third-party Faust implementations. I asked. The response was,
"What would be the point?" I felt I had evoked a defensive response,
and decided not to pry much further.

There's an opportunity to serve the niche of DSP languages better. Faust
originated as a research project and seems not entirely aware that its
goals differ significantly from that of a production-grade product.

A production-grade DSP language wouldn't have _ideas_ about audio, user,
and system interfaces. It wouldn't pretend to enforce contracts that it
has to delegate to somebody else. It would be compatible with whatever
kind of memory allocation the application needs. It would generate
idiomatic code for the target language. It would restrict itself to a
minimal core of well-designed, critical features and adhere to a formal
specification that promotes standardization. It would exhibit good version
control discipline with useful commit messages and terse diffs. It would
document all interfaces, both programmatic and linguistic. It would
make people feel welcome by setting clear expectations of participants'
conduct.  It would support other open source projects by choosing them
as solutions for its own needs. And it would support simple, interactive
development tools that do not require network access to function.

I don't doubt that if such a project existed, it would provide great
benefit to many people.


