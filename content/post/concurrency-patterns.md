---
title: Concurrency Patterns in Go
draft: true
---


A **generator** is a function returning a read-only channel, which internally
launches a goroutine that sends on that channel.

	func Lexer(r io.Reader) <-chan string {
		words := make(chan string)
		go func() {
			defer close(words)
			line := bufio.NewScanner(r)
			for line.Scan() {
				for _, word := range strings.Fields(line.Text()) {
					words <- word
				}
			}
		}()
		return words
	}

This is a tokenizer that I often use (with certain modifications) for creating
stack-based programming languages. It runs concurrently with the parser, which
consumes the token stream.

It's not the most flexible tokenizer in the world (all tokens must be
separated by whitespace), but it's extremely simple.

If the function needs to return more than just a channel (say, an object with
a bit more state), that can easily be accomodated.

	type Lexer struct {
		Words chan string
		line *bufio.Scanner
		// add more fields as needed
	}

	func NewLexer(r io.Reader) Lexer {
		return Lexer{
			line:  bufio.NewScanner(r),
			Words: make(chan string),
		}
	}

	func (l Lexer) Run() {
		defer close(l.Words)
		for l.line.Scan() {
			for _, word := range strings.Fields(l.line.Text()) {
				l.Words <- word
			}
		}
	}

What's interesting about this is that we can separate the initialization
of the lexer from its execution. By writing the lexer algorithm as a method,
we also allow clients to control how to run it; either in the current
goroutine (`l.Run()`) or in another one (`go l.Run()`).

In larger codebases, it may be a good idea to encapsulate the channel in
a method. This allows us to ensure that clients can't send on the channel.

	func (l Lexer) Output() <-chan string {
		return l.words
	}

Some Go programmers discourage exposing channels in APIs. However, needlessly
hiding channels can make it difficult for clients to write correct concurrent
code. Without exposing channels, it's impossible to use a concurrent interface
in a `select` statement, severely limiting how the API can be used.

Wrapping the channel in a method also allows the `Lexer` type to satisfy an
interface, enabling modularity. It can be very easy to construct and modify
data processing pipelines with this approach.

Using methods also means that we can track down the behavior of a channel
to a particular type or method declaration. Whereas, a channel type alone
might not elicit its behavior very well.

## Serialization

I'm not refering to marshaling or encoding. I'm talking about serializing
access to shared data to prevent races.

Mutexes are very performant, but avoid them until you know where your
bottlenecks are. Channels provide a more comfortable interface.

	type Serializer struct {
		in, out chan int
	}

	func NewSerializer() *Serializer {
		s := &Serializer{
			in:  make(chan int),
			out: make(chan int),
		}
		go s.run()
		return s
	}

	func (s *Serializer) run() {
		state := <-s.in
		for {
			select {
			case state = <-s.in:
			case s.out <- state:
			}
		}
	}

	func (s *Serializer) Set() chan<- int { return s.in }
	func (s *Serializer) Get() <-chan int { return s.out }

Sometimes it's not just getting and setting that need to be serialized.
In some cases, clients really need to be able to do an atomic read-write
operation, such as test-and-set. To do that, we can use a channel of functions.

	type Serializer struct {
		op chan func(int) int
	}

	func NewSerializer() *Serializer {
		s := &Serializer{make(chan func(int) int)}
		go s.run()
		return s
	}

	func (s *Serializer) run() {
		var state int
		for {
			select {
			case op := <-s.op:
				state = op(state)
			}
		}
	}

	func (s *Serializer) Mutate() chan<- func(int) int {
		return s.op
	}

I'll admit, however, that I've never needed this. If you need to do mutate
some state, and that state is in another goroutine, it may suggest that your
program isn't well organized.

Another approach to this is to use a channel of pointers, with the assumption
that the channel is the only way to access the state, and that at any given time, at most one goroutine has any given pointer.

	zeroer := func(in <-chan *int) {
		for i := range in {
			*i = 0
		}
	}

It seems much easier to get this sort of code wrong, however.

## Queues

A buffered channel is a queue. Sender and receiver can be the same goroutine.

	l := 10
	q := make(chan int, l)

	for i := 0; i < l; i++ {
		q <- i
	}

	for i := range q {
		q <- i * i
	}
	close(q)

This can be useful when some relatively static code, like reading a directory
list, needs to interface with more dynamic code, like a pipeline. The queue can
be initialized without launching a goroutine (and without lazily producing
the values to be sent), yet can still be connected to the pipeline.
