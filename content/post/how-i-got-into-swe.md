---
title: How I got into software engineering
date: 2024-07-05
---

I didn't plan to become a software engineer, and the story of how I got to be one may be interesting to you.

# '90s

Electronics was one of my earliest interests. I learned by taking things apart. Later came the ability to put them back together. I also remember making a robotic line-follower kit with my dad and I had the first LEGO Mindstorms. I wasn't good at programming it, but I liked the idea of programmable electro-mechanical LEGO models.

# '00s

My formal education in electronics began in high school, where I chose to spend half of my day at Berks Career and Technology Center. This was a good environment for me --- it allowed me to focus on something I was passionate about, and reduced the social stress of being a teenager. I was lucky to have had Mr. Leonard Krug as my instructor, who recognized my love of electronics and gave me freedom to challenge myself with work that wasn't part of the usual curriculum.

Around the same time, I got into electronic music and wanted to understand how it was made. I was intrigued by voltage-controlled filters, but my instructor didn't know how to make one. Eventually, I found an OTA-based, four-pole design and prototyped it on a breadboard. I was thrilled when I first heard it!

After graduating high school, I went to Penn State Berks with the plan to major in electrical engineering. However, I struggled in my first semester. I loved calculus; I'll never forget the teacher, Valetta Eschbach, who, in response to my questions about limits and derivatives, remarked with a smirk, "You're my philosopher." But it was a very big change from my previous routine. This and other stresses at the time caused me to drop out.

I wasn't sure what the problem was. I tried enrolling at a community college, but I was not motivated to succeed there. I dropped out again.

At age 20, having dropped out of college twice, I was depressed and felt that I had nothing to lose. My mother had recently remarried and invited me to come with her to live with her new husband in the state of Washington. That's what I did.

I had no plan upon arrival, but quickly decided I needed to try something different: I enrolled at the Art Institute of Seattle in their Music Production program. Music had become a hobby of mine and my electronics knowledge would be somewhat useful if I became a mixing engineer.

I did really well there until about 1.5 years into the program, when I encountered a sound replacement for video class that kicked my ass. I had no interest, which is my Achilles heel. The instructor formed a low opinion of me and I felt crushed. I also had realized by this point that I couldn't stop thinking about the internals of the audio gear we were using. I wanted to make the technology, not use it.

# '10s

I dropped out and eventually realized that I had to go back to school for electrical engineering, but that it would require perseverence. Fortunately my brain had become a bit more developed; coming at the problem with better executive function was probably a factor in my success. And yes, in 2015, I graduated.

During my last run at college, my curiosity bloomed. I studied a lot of things outside of class, especially mathematics and philosophy. I felt out-of-place and wanted to find a theory of education that worked for me.

My interest in math led me to computer algebra systems, which amazed me. How did Wolfram Alpha work? I enjoyed going to the library to use one of their iMac computers that ran Grapher. I wanted more power, however. I couldn't afford Mathematica.

I learned about Sage. At the time (around 2012), it ran only on Linux. I tried to install Debian on my computer, but it didn't work; I think there was a driver problem. It wasn't until I had gotten married that I could afford to build a new computer on which to run Linux.

My first distro was Arch Linux, which may surprise you! It's not usually recommended for beginners. Despite having no prior experience with Linux, I picked it up quickly. I thank my friend Josh Smith for giving me the initial support to succeed.

Adopting Linux was one the best choices of my life. It was so empowering and an enabling factor to me becoming a software engineer. It provided a transparent and safe learning environment where I could identify my own goals and be free to make mistakes.

But I didn't become one right away. First I had to find a job. I looked for a year and was finally hired as a System Design Engineer at ESG Automotive in Troy, Michigan. I moved 500 miles. I didn't enjoy the work. As a Linux user, I was frustrated to find myself in a Windows-saturated world.

One of my few impacts in my first job was to get the team using Git to version the firmware we were prototyping. I didn't want to put company code on my personal GitHub account, however, so I asked around at ESG to find out if we had an organization. That connected me with Devon Bleibtrey, who hooked me up with access to ESG's GitHub org.

Eventually, my project at Ford Motor Company turned for the worse and I had to leave. Fortunately, ESG tried pretty hard to find me another job internally. I showed up wearing a suit for what I thought was an interview with Devon! In fact, it was more of an onboarding. The job was interesting: they wanted me to write a CPU profiler for embedded Linux apps. I suspect that Devon was already impressed when he learned I was an embedded systems person who wanted to use Git. That's a pretty low bar.

That's how I got into "pure" software engineering. Devon came from a different background: web development. But with him as my manager, I learned quickly and got along well with my teammates. I learned a lot about software engineering principles in that role and emerged from it with a love of the field.

What I love most software engineering as a career is its capacity for fast iteration. As of today, I've refined this into a set of best practices that I try to implement in each of my projects. When you're iterating quickly, you can stay in a flow state; solving problems like this is one of the most enjoyable experiences of my life.

I tried a few different career paths before settling into software engineering, and it was somewhat of an accident. However, it stemmed out of my curiosity rather than a standardized, preconceived career track. Perhaps if I had grown up in a Linux-using household, my young adulthood could've been less turbulent. My four-year-old son, who is showing many of the characteristics I did at his age, may be in a better position when it's time for him to choose a career. I'll do what I can.
