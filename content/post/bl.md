---
title: Small is beautiful
date: 2021-07-27T03:50:15-04:00
---

[bl][] is a tiny backlight adjuster I wrote for myself. I've used it
for years without needing to change it very much. When I am feeling sad,
I enjoy reading its codebase.

[bl]: https://git.sr.ht/~kdsch/bl/tree


The tool was first written in Go and later rewritten in C for the sake
of minimalism. A neat way to see its simplicity is to run it with strace
and count the lines of the output.

	$ strace -o /dev/stdout bl + | wc -l
	14

Its build system, designed to reduce size and dependencies, generates 33
kB. Its tests achieve clarity by following an unusual [convention][].
bl adjusts smoothly at any brightness level, since it scales by a
constant factor.

[convention]: /post/unit-tests-c/

bl is ~~im~~perfect. I didn't resist the urge to add a new feature, only to
never use it. ~~For now, the feature stays, warning me of myself.~~

