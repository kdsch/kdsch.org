---
title: How to liberate billions of Android users
date: 2021-07-10T12:42:19-04:00
---

As of 2019, there were [2.5 billion][ap] active Android
devices. Typically, software vendors such as Google, Samsung, LG,
and Motorola only support devices for two or three years. After
that, vendors _drop_ software updates, a complaint motivating the
development of [postmarketOS][pmos], a Linux distribution targeting
mobile devices. Users' ability to adopt such community-controlled
software is hindered, however, by vendors' persistent _grip_ on
bootloaders---refusal to unlock them to allow installing third-party
software. Vendors act like they [own][vendor] these devices.

[ap]: https://www.androidpolice.com/2019/05/07/there-are-now-more-than-2-5-billion-active-android-devices/
[pmos]: http://postmarketos.org/
[vendor]: https://drewdevault.com/2020/06/26/Vendor-purpose-OS.html

When vendors **drop and drag**, they want users to choose from a limited
set of options:

1. **Keep using the device** despite outdated software. The unpatched
   security vulnerabilities expose the user to risk of identity theft,
   ransomware, surveillance, and denial-of-service, and may facilitate
   exploitation by remote agents for cryptocurrency mining or botnets.
   The vendor promotes malware through its negligence.

2. **Convert the device into [e-waste][ifixit]**. A recycler destroys
   it or a waste collector puts it in a landfill. The vendor thereby
   wastes the energy and materials used in manufacturing and cuts short
   the remaining service life of the hardware, which is often several
   years. Vendors prefer this option, as it encourages people to buy a
   new device, which is their business model.

[ifixit]: https://www.ifixit.com/Right-to-Repair/E-waste

Observe this negligent practice and the upward trend of unrepairable
design, and it is clear that vendors do not care about users or the
environment. They just want to sell devices.

I see three prongs of counteraction.

**Lobby governments to outlaw drop-and-drag**. Make vendors treat
consumers and the environment better. This will take time, as the
vendors have powerful political lobbies of their own, and work very
hard to maintain a false [image of sustainability][forbes]. A strong,
bottom-up democracy is a prerequisite of this approach, which, in the
United States, presents a significant obstacle.

[forbes]: https://www.forbes.com/sites/blakemorgan/2020/11/09/10-most-sustainable-consumer-tech-companies/?sh=59d21ee249a8

**Create community-controlled technology.** Set an example by
producing devices that are meant to be controlled by users, ideally
with [copylefted][cp] hardware and software, which will ensure that
the intellectual commons around mobile computing is replenished and
remains in the best interest of users. While this approach can spread
community-centered thinking about technology, it does not directly
counteract the impact of existing proprietary, authoritarian technology.
Mobile devices are highly complex, and much of the knowledge around them
is proprietary.

[cp]: https://drewdevault.com/2019/06/13/My-journey-from-MIT-to-GPL.html

These paths should be taken; I think they are essential to achieving
resilient community control over technology. However, they are indirect
and face significant obstacles which will delay their impact.

There's a third option available to users, which vendors know most
will not take:

**Exploit vulnerabilities in the device** to unlock the bootloader and
install community-controlled software. Most users lack the skill to do
this, but some try by using third-party software---often obtained on
forums full of people seeking ways to customize their device. Users that
cannot evaluate the exploiting applications risk exposure to malware.

This path is accessible to me, since I am a software engineer. Lacking
a background in computer security, I thought unlocking a bootloader
by exploiting vulnerabilities would be a great learning experience. I
obtained a Motorola Moto G4 Play (XT1607) on eBay for $30. This model was
released in 2016, so Motorola dropped it years ago. After I discovered
that the phone was sold under a deal giving Amazon control over the
software, and that Motorola was dragging it by the bootloader---declining
to unlock it at my request---I began seeking vulnerabilities and
exploits. The process consumed a lot of time and energy, which prompted
me to step back and devise a more strategic approach.

Understanding the vulnerabilities that affect a device in-hand requires
obtaining as much information as possible about that device, such as
security patch level and kernel version. As long as the device boots,
this data can be obtained from the phone UI itself and using
`adb` and `fastboot`.

Android regularly publishes [security bulletins][sb] that, while referring
to a lot of information that isn't public, still provide useful details
on many bugs and patches. The bulletins are available only as HTML on
the web, so they're not suited to searching for vulnerabilities that
affect a specific device.

[sb]: https://source.android.com/security/bulletin

To deal with this obstacle, I created [android-bulletin][ab], a tool
that transforms the 3000-some vulnerabilities listed in the bulletins
into a single table. [textql][] allows the table
to be queried using SQL, the _lingua franca_ of data. For instance,
to search for vulnerabilities in my XT1607, I wrote the query

[ab]: https://git.sr.ht/~kdsch/android-bulletin
[textql]: https://github.com/dinedal/textql

	select * from vulns
	where patchlevel between "2018-06-05" and date("now")
	and not reference = ""
	and (versions like "%7.1.1%"
	    or (component like "%Qualcomm%"
	       and component2 like "%boot%")
	    or (component like "%kernel%"
	       and component2 like "%kernel%"));

which returns a table of about 200 vulnerabilities that may affect the
device. Due to the complexity of the Android software ecosystem, the
device is not guaranteed to have _all_ vulnerabilities listed. But at
least I don't have to sift through thousands of vulnerabilities by hand.

The table can also be loaded by standard spreadsheet software, and
filters can be used to produce similar results.

I'd like to see unlocking bootloaders transformed from an individual
problem---something done by enthusiasts---into an organized, communal
activity that makes it accessible to more people. Instead of seeking out
shadowy [exploit apps][sunshine] in a forum, there should be a community
of people collecting knowledge and tools that can automate the process of
unlocking bootloaders. Communities like postmarketOS might be well-poised
to undertake such an effort.

[sunshine]: http://theroot.ninja/

There are challenges to this approach; the number of software and hardware
variants of the 2.5 billion devices is very large. But by pooling our
efforts and creating tools that leverage computation, maybe we can unlock
some of them, and free their users from the drop-and-draggers who made
this mess.
