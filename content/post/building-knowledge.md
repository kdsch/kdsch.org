---
title: Knowledge is made in curious ways
date: 2021-10-22T04:57:29-04:00
---

Since I began writing ode2dsp (mentioned [previously](/post/ode2dsp)),
it has been refactored a few times, and has achieved what it set out
to do. Success! Maybe.

When ode2dsp was first conceived, it was meant to be a tool to prevent
silly mistakes in creating models. Having built this, it was not difficult
to add other features that leverage the existing data structures---namely,
computing stability conditions. I expected this to enable developers
to anticipate stability issues in generated models.

For stability conditions to be useful, they have to calculated as
closely as possible with respect to the generated code. This meant
ode2dsp couldn't delegate Newton's method to the target code any longer;
it has to compute it itself, otherwise it can't calculate the stability
condition. Although I implemented stability analysis for the entire
mathematical pipeline---from ODEs to FDEs to approximated FDEs that map
one-to-one to target code---ultimately, when the target code is diverging,
only the last part of the pipeline matters.

Stability conditions are most useful when they are simple, which is
rare. Generally, they are difficult to visualize because they depend on
many variables. Sometimes, SymPy is not able to devise a simpler form,
and this doesn't mean one doesn't exist. Stability analysis may benefit
from manual tinkering. But it is more realistic to assume the developer
doesn't want to tinker; that they will see a big, complicated expression
and assume the model has an operating point-dependent instability,
which is quite common. And they'll confirm this not by trying to plot
the stability condition, but by running the model and looking at the
waveforms, just like in the 1960s.

The relationship between a stability condition and the upstream
definitions is not straightforward. It is difficult to understand how
to improve a stability condition; how to change something upstream of
it so as to reduce it or make it unconditionally true. I don't have
that kind of insight, and would think it unwise to confront that kind
of complexity head-first.

Then I got an idea: what if we abandon the pipeline---stop modeling
ODEs---and write stable FDEs directly? What if we parameterize the
set of stable linear FDEs, then "nonlinearize" them by making their
parameters functions of state, so as to guarantee stability? I've started
investigating this, and it's too early to say whether it'll pan out,
but I feel optimistic.

There's an insight here, which isn't about math, but people: I wanted
to _specify_ the stability condition, not calculate it. Stability turned
out to be a hard requirement once I couldn't take it for granted. Having
learned this about myself, the old code won't be needed any longer,
despite over two months of development. I thought I was building a tool,
but really I was building knowledge.