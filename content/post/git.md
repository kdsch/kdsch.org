---
title: Take advantage of Git
date: 2023-05-10T10:15:28-04:00
---

Git is awesome, but delivers most of its value when used beyond the utter
basics.

## Write multi-line commit messages

I've seen some codebases that make me wonder if the developers know that
`git commit -m` isn't the only way to commit changes.  Some changes are
too complex to summarize in one line.

I never use `-m` anyway because I want to heed the commit message style
guidelines, and my editor helps me do that.


## Store docs with code

I think teams have bad reasons to avoid this.

- They pay good money for a team collaboration suite and are determined
  to get value out of it, even though it provides no way to synchronize
  code and docs.

- They've got a bad habit of using rich text formats for documentation. Most
  code forges support Markdown rendering, so there is little reason to use
  a file format inspired by paper.

- I have encountered repositories with rich text documents checked in, which
  makes the diffs meaningless. Git works best with plain text source.

- They think by hosting it in a Git repository, they make it inconvenient
  for others to access. If learning to browse files on a code forge is too
  difficult for people outside the team, run a CI job that converts and
  publishes the docs for general consumption.


## Use branches for parallel versions

I've seen some teams creating new versions of something by creating a
new repository and copying files into it. If you use the same repository
and create a new branch, your job is done.


## Minimize diff noise

Clean diffs are easier to summarize.

- Never use `git commit -a`. Name the specific files you mean to add.

- Never use shell globs to add files, as in `git add wrong/*.file`.

- Use `git commit -v` to review the diff before committing. If you can't
  summarize the changes, you're probably committing too much.

- Don't use comments to disable and preserve old code. Use `git log -p` to
  find old code that might be useful today.


## It's not a bucket

A Git repository isn't a place where you dump stuff. It's a way to
version and distribute important information. Treating it as a sack for
your disorganized bits and bobs will cause pain in the future.
