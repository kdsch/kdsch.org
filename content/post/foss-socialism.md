---
title: "FOSS and socialism"
date: 2022-02-08T03:46:00-05:00
---

Sometimes comparisons are drawn between the free software movement
and socialism. In an article about software licensing, Drew DeVault
described free software as a "[socialist utopia][dd]... ripe for
exploitation by capitalists". Andrew Keen accused Lawrence Lessig, a
prominent free-culture activist, of being an ["intellectual property
communist"][ipcom]. An article published in Jacobin Magazine claims
"[free software isn't socialism][j] for your computer". What is the
relationship between socialism and the free software movement?

[dd]: https://drewdevault.com/2019/06/13/My-journey-from-MIT-to-GPL.html
[ipcom]: https://en.wikipedia.org/wiki/Free-culture_movement#Copyright_proponents
[j]: https://www.jacobinmag.com/2016/02/free-software-movement-richard-stallman-linux-open-source-enclosure

Full disclosure: I'm a member of [Democratic Socialists of America][dsa].

[dsa]: https://www.dsausa.org/

The complexity of free software and socialism makes a definitive
relationship impossible. However, we can use claims like those above as
point to stimulate our discussion. Before that, let's clarify some terms.


## Definitions

Socialism has no single definition, but Stanford Encyclopedia of
Philosophy has a [useful characterization][plato]:

> Both socialism and capitalism grant workers legal control of their
labor power, but socialism, unlike capitalism, requires that the bulk of
the means of production workers use to yield goods and services be under
the effective control of workers themselves, rather than in the hands
of the members of a different, capitalist class under whose direction
they must toil.

[plato]: https://plato.stanford.edu/entries/socialism/

Free software, however, has a clear [definition][], which I will not
reproduce here. Read it if you are not sure.

[definition]: https://opensource.org/osd


## Keen's communism

Keen's comment, while it does not mention the free software movement,
still applies to it because it pertains to the free culture movement,
which includes free software.

Keen is an entrepreneur and critic of "Web 2.0". His main beef seems to be
that the Internet provides people not fortunate enough to have received
a certain kind of education the chance to be listened to. Though I don't
know his official political affiliation, his defense of the previous
social order (in which a small number of people had greater control of
media production) suggests a conservative disposition. His opposition
to social change entailed by tech enterprise, however, is not inherently
conservative.

Keen's argument against free culture reuses an old defense of inequality:
Equality is Mediocrity. Therefore, his argument inherits the weaknesses
of that cliché, and is susceptible to its counterarguments.


## DeVault's socialist utopia

DeVault's political position is clear. While he too is an entrepreneur,
he has [critiqued][] capitalism as ultimately about self-enrichment at
the expense of ethics. He claims to have crafted his business model to
yield more consumer-centric incentives. In particular, he [rejects][]
outside investors and makes services [freely][] available to customers
who explain why they cannot pay. For DeVault, socialism is about
circumventing markets to broaden distribution of software benefits,
and enterprise is about meeting the constraints of capitalist society
(acquiring a minimum of goods on a finite budget) to attain social ends,
such as breadth of distribution.

[critiqued]: https://duckduckgo.com/?t=ffab&q=capitalism+site%3Adrewdevault.com&ia=web
[rejects]: https://sourcehut.org/blog/2019-10-23-srht-puts-users-first/
[freely]: https://man.sr.ht/billing-faq.md#i-dont-think-i-can-pay-for-it

There is much evidence to support a view of DeVault as a _social
entrepreneur._ He emphasizes both business model as means to social
ends and consumer choice---voting with your dollar---as a means to
ethical ends. He's also made a conscious personal choice to devote his
life to free software instead of politics, going so far as to [emigrate]
to the Netherlands for political refuge.

[emigrate]: https://drewdevault.com/2021/06/07/The-Netherlands.html

Not all socialists criticize privileged individuals such as DeVault for
exercising their personal freedom, in part because the exercise of such
privileges, though they may be inequally distributed, is not always seen
as an obstacle to socialism. Indeed, socialists advocate for a world in
which everybody has such freedoms; rather, they oppose a system that
keeps it only for the few.

A more common factor, regarding socialists' judgment of DeVault, is
his status as an entrepreneur who potentially profits from others'
labor.  Social entrepreneurs are a mixed, sometimes slippery bunch in
terms of their political allegiances, and socialists tend to receive
them with skepticism. Nonetheless, socialism is not incompatible with
enterprise. A reliable way to tick off socialists is to oppose worker
empowerment; worker-owned cooperatives are the preferred business
structure. Nevertheless, few socialists see starting cooperatives as
a _strategy_. Given the small scale of DeVault's operation, he's not
likely to take much heat from socialists, who reserve their vitriol for
the most accomplished labor exploiters.


## Jacobin's free software movement

Both of these invocations of socialism focus on consumption. Yet
socialists see production as the main turf on which capitalist power
can be challenged. Capitalists own the means of production, and depend
on workers to operate it.

Thus the concern for socialists is how software freedoms relate to
the broader project of worker empowerment. Free software undoubtedly
achieves a more equal distribution of software than would otherwise exist.
However, most free software authors are not paid for their work, relying
on income from other sources. It is widely acknowledged that while free
software isn't non-commercial, it's very difficult to monetize. Many open
source projects are sustained on donations; most have no income at all.
Free software workers, like all technology workers, are not invulnerable
to exploitation.



Theoretically, both movements are egalitarian: they want to achieve some
kind of equality. Socialists focus on material equality, and propose
social policies that tend to elevate ordinary people. Free culture grants
freedom to consume and produce in equal measure to everybody.

The similarities would end there, if not for copyleft licensing, which
is free software's strategy to defend the commons against enclosure
by exploiting copyright law. Socialists too, oppose enclosure of the
commons ("privatization"), but because their purview is society as
a whole, not just the informational artifacts of cultural production
("intellectual property"), they rely on political organizing. This is
not to say free software has had no [political dimension][momi]. Rather,
free software grew to a dominant position without modifying existing
political institutions.

[momi]: https://momi.ca/posts/2020-07-27-politicaltech.html

Evidently, the aims of free software are not outright anti-capitalist,
otherwise the movement would not encompass such an array as venture
capitalists, technology workers, and anarchist hackers. Free software,
in its broadest sense, embraces all these people as legitimate users
and contributors, if only because free software licenses ensure their
equality.

Socialism, in contrast, embraces workers over capitalists. It is not
opposed to enterprise; it is opposed to enterprise that diminishes worker
power. Socialists see worker-owned cooperatives as a way to empower
workers, but their strategy for growth involves political organizing,
such as unions and electoral campaigns, rather than entrepreneurism.

It's clear that capitalism has embraced free software, to some extent,
although not without ill intent. A common concern is that businesses
are trying to coopt the movement for their own benefit. It's unclear
whether free software licenses are sufficient to resist this.

Free software hinges upon copyright law; it hijacks a system meant to
enforce intellectual property rights. In this way, it plays a role
similar to [cooperatives in Denmark][denmark], which hijack private
property laws to prevent housing and infrastructure from being sold to
capital.

[denmark]: https://jacobinmag.com/2021/10/socialism-state-ownership-redistribution-power-cooperatives-neoliberalism-social-democracy


## In summary

All strains of the free software movement embrace the four freedoms. As
such, free software tends toward a consumerist value which has no
direct relationship to replacing capitalism or empowering workers, even
if it may result in more equitable distribution of software benefits.
Socialism is a project to replace capitalism by shifting the balance of
power in favor of workers. Insofar as free software exemplifies strategies
to resist enclosure of the commons, socialists may find it inspiring.

Speaking personally, it was as a technology user that I came to appreciate
free software. It was as a technology worker that I came to appreciate
socialism. Could it have been the reverse? Would being a technology user
make me appreciate socialism? Not obviously. Would being a technology
worker make me appreciate free software? Yes; it could make my job more
convenient, make me feel better about the work I do.

It becomes clear why free software isn't antithetical to capitalism. It
can serve as an impotent concession to workers. It potentially heads
off an objection to the concentration of ownership. It allows a company
to brand itself ethically without granting any actual decision power
to workers.
