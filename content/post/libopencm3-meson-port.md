---
title: A port of libopencm3's build system to Meson
date: 2023-05-02
---

I am [porting][port] [libopencm3]'s build system to [Meson]. I've used
Make for a long time. Here's why I want to use Meson for new projects.

[libopencm3]: http://libopencm3.org/

- Meson puts _me_ in charge of dependency integration, not some vendor or SDK.
  I don't want to copy files into my source tree or be tied to some
  "workspace" that an IDE requires. I want to point at a Git commit.
  Git submodules are too hard to delete. I've [written][wraps] about this.

- The Meson language is well designed and specified, inspired by Python
  rather than shell. Two implementations of Meson exist: Meson proper,
  written in Python; and [Muon], written in C. These are a sign of
  maturity.

- Meson suits the needs of a microcontroller project. It has good support for
  cross-compilation. It supports only out-of-tree builds, so creating and
  comparing builds with different configurations is easy.

- The Meson port builds code at a speed similar to the original build
  system. When building all targets, Meson is only slightly faster. When
  building a subset of targets, Meson is faster due to better dependency
  tracking for generated interrupt code. (Both build systems have a
  syscall bottleneck, which I believe is due to filesystem I/O.)

- A project using Meson can add CMake projects as dependencies. CMake is
  used by some popular embedded projects, such as FreeRTOS.

[Muon]: https://muon.build
[wraps]: https://kdsch.org/post/meson-wraps

## Status

The Meson port builds all targets. It supports building a subset of
targets when used as a Meson subproject. I've been testing the STM32F4
target on an [example project][sampler]. This project has been running
well on the stm32f4-discovery development board. I have not run code for
other targets. The port does not yet support running libopencm3's tests
or building Doxygen documentation. The port does not interoperate with
libopencm3's [examples] or [template] repositories.

[examples]: https://github.com/libopencm3/libopencm3-examples
[Meson]: https://mesonbuild.com
[port]: https://git.sr.ht/~kdsch/libopencm3
[sampler]: https://git.sr.ht/~kdsch/sampler
[template]: https://github.com/libopencm3/libopencm3-template

## Goals

1. Don't break the original build system.

2. Avoid changing existing files. Adding new files is OK.

3. Write idiomatic Meson code.

4. Support operating as a Meson subproject.

5. Match the original toolchain invocations as closely as possible.


The port was created by manual and automated processes. An [Awk
script][awk] translates target Makefiles into Meson code. To simplify
the script, I changed a few Makefiles to be more uniform. Meson assumes
that source files with a .c extension are valid translation units.
This isn't true in the case of vector_nvic.c: it is not compiled itself,
but is included by another file. I [worked around][workaround] this
by changing the filename to vector_nvic.h.

[awk]: https://git.sr.ht/~kdsch/libopencm3/tree/584d52ac5f788711862301da05439455d406502f/item/scripts/make2meson.awk
[workaround]: https://git.sr.ht/~kdsch/libopencm3/commit/3e96ec1b6201ac71edfe19ce47a1baca048bd15a

The port has four build-time dependencies: Meson (obviously), Python
(which was already used), [Ninja], and Awk. The Awk script is checked
in.  (It may be removed if the two build systems no longer need to be
synchronized.) Ninja is widely available. There is also a Ninja-compatible
alternative called [Samurai], written in C.

[Ninja]: https://ninja-build.org
[Samurai]: https://git.sr.ht/~mcf/samurai


If upstream were to consider accepting Meson support for libopencm3,
they should think about:

- Do we want to have two build systems, or just Meson?

  When Qemu ported to Meson, it [removed][qemu] its original build system
  for fear of increasing maintenance burden on contributors. Qemu's at
  least ten times bigger than libopencm3 and much more complex, so this
  isn't surprising.  libopencm3 may or may not want to have two build
  systems. If we remove the original build system, we'll have to port
  (or abandon) the examples and template repositories.

  Removing the original build system might allow a different source tree
  structure. libopencm3 uses the C preprocessor to control what code
  is part of a target. With Meson, there might be other options.

[qemu]: https://wiki.qemu.org/Features/Meson/Design
