---
title: Work with Git submodules
date: 2023-05-17T14:39:16-04:00
---

## Add a submodule at a specific commit

	git submodule add https://git.sr.ht/~kdsch/bl
	git -C bl/ checkout 113632ca
	git add bl/
	git commit -m 'add bl submodule at 113632ca'

## List submodules

	git submodule

## Change the version of a submodule

	git -C bl/ checkout f81b3582
	git add bl/
	git commit -m 'bl: update to f81b3582'

## Change the remote of a submodule

	git submodule set-url bl git@git.sr.ht:~kdsch/bl
	git add .gitmodules
	git commit -m 'bl: use ssh protocol for the remote'

**Note** `git submodule set-url <path>` will not work correctly if the
path contains a trailing slash.

## Delete a submodule

This used to require several more commands.

	git rm bl/
	git commit -m 'delete bl submodule'

