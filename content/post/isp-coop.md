---
title: The Case for a Cooperative Internet Service Provider
date: 2018-06-10
---

As the Internet has evolved, new services have challenged the engineering and
business assumptions of existing networks. [BitTorrent and Comcast][bt],
[Skype/iPhone and AT&T][sky], [Netflix/Level 3 and Comcast][nf] are just a few
cases in which changing relations among Internet organizations have caused
consumer dissatisfaction.

[sky]: https://www.wired.com/2009/10/iphone-att-skype/
[bt]: https://en.wikipedia.org/wiki/BitTorrent#Throttling_and_encryption
[nf]: https://en.wikipedia.org/wiki/Level_3_Communications#Comcast_dispute

Consumers rightly see the risk in giving ISPs too much power.  However,
they typically lack insight into the technical and business situations
that ISPs confront, for example, [routing][route] and [peering agreements][pa],
making it easy for the actions of ISPs to be seen as Orwellian.

[route]: https://en.wikipedia.org/wiki/Routing
[pa]: https://en.wikipedia.org/wiki/Peering#Peering_agreement

Consumers widely support the position of net neutrality, which maintains that
ISPs should not be permitted to change the kind of service provided based on
its content. Arguments against net neutrality are not commonly entertained,
as though they were indefensible.

What's interesting is that upon closer inspection, there are reasonable
arguments against net neutrality, such as those discussed in a video by [Ben
Eater and Grand Sanderson][opp]. ISPs are in a difficult position. On one
hand, they provide services to customers in terms of bandwidths. For instance,
you can buy a 50-Mb connection for a fixed monthly cost. On the other hand,
the amount of bandwidth usage varies with time, which means that the quality
of service delivered to customers is lower during peak usage times.

A further complication to the ISP's job is that of maintaining relationships
with other ISPs. The Internet, despite its name, is not a monolith, but
a complex arrangement of organizations whose relationships change dynamically.

Could it be that the problem the Internet is facing is simply the classic
_tragedy of the commons?_ We have a common resource which is potentially abused
by consumers. It is stewarded by a private entity which is in a compromised
position; it cannot practically charge consumers for the consequences of its
use. So what it does instead is charge _producers._

[opp]: https://www.youtube.com/watch?v=hKD-lBrZ_Gg

If the Internet _is_ experiencing such a tragedy, it might benefit from research
on [ways that the tragedy of the commons has been avoided][avoid]. 

This supports the following hypothesis: Society is better served
by a greater investment by communities into the management of Internet services.

[avoid]: https://en.wikipedia.org/wiki/Tragedy_of_the_commons#Non-governmental_solution

## Why a Cooperative?

A cooperative is a business democratically controlled by its owner-members. The
members might be workers, consumers, or both. In that regard, cooperatives are
flexible, but the aspect of democratic control is essential.

Because of this control structure, many cooperatives merge the roles of producer
and consumer. This provides a more direct way for the system to account for the
conseqeuences of its own activity. Instead of having to charge either data
consumers or content producers for the consequences of their usage, it would
have Internet _users_ directly responsible for addressing those consequences.
Having this responsibility also entrusts them with a degree of power.

Cooperatives are often established to meet a shared need.

An example of this kind of structure is the [Park Slope Food
Coop](https://vimeo.com/29787733) in New York City. It formed in order to
provide higher quality food at a lower price to members of its community. The
most expensive part of running a grocery store is the paid labor. To reduce
this cost, each member works a few hours a month. Members work with the same
team each time in order to facilitate self-management.

Such a structure distributes responsibility directly; there is no financial
transaction required. Running the store is built into the protocol that
members must adhere to. This allows the system to optimize for stability and
quality rather than profit. The bottom line is a concern (every system has a
budget constraint) but the way that it addresses the concern is different.

## A Community-Scaled Approach to Telecom

What would a cooperative Internet service provider look like? There are many
challenges to a community starting an ISP. ISPs typically require a lot of
startup capital. Most seem to be derived from relatively old, large businesses.

ISPs need network infrastructure. Many kinds of infrastructure are possible:
buried cable, fiber optics, and wireless are the most widespread, and the most
typical in backhaul applications.

One infrastructure technology that is less typical is free-space optics.
[Free-space optical communication][fso] is an optical communication technology
that uses light propagating in free space to wirelessly transmit data for
telecommunications or computer networking. The technology is useful where other
media, such as wire, are impractical due to high costs or other considerations.

The [RONJA][ronja] project in Europe has demonstrated that FSO technology can
simplify running an ISP. RONJA transceivers can be produced by hand, allowing
hobbyists and grassroots organizations to create network infrastructure that is
independent from existing buried cable and not subject to RF regulations.
Furthermore, RONJA's hardware design is open source, so users can adapt the
design to their own situations.

[ronja]: http://ronja.twibright.com/about.php

Free-space optics provides an opportunity for local, community-managed
Internet service. It lowers the barrier to entering the telecommunications
market by providing a simple, low-cost, high-bandwidth link between network
nodes.

[fso]: https://en.wikipedia.org/wiki/Free-space_optical_communication

Unfortunately, RONJA production is a bit labor-intensive. One way to reduce the
amount of labor is to adapt off-the-shelf networking hardware; use existing
electronics and mate it to custom optics.  For example, media converters that
translate from wired Ethernet to fiber optic media could be connected to an
optical collimator. The collimator focuses the high-divergence beam emitted by
the optical fiber into a low-divergence beam suitable for free-space
propagation.

## Similar Projects

- [guifi.net](https://en.wikipedia.org/wiki/Guifi.net)
- [Magnolia Road Internet Cooperative](http://magnoliaroad.net/). See also [The
  Future of Owning the Internet](https://www.vice.com/en_us/article/exq3ne/zen-and-the-art-of-internet-maintenance-v23n1)
- [Freifunk](https://freifunk.net/en/)
