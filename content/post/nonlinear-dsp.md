---
title: Nonlinear DSP is good, fun, and hard
date: 2021-08-12T13:40:26-04:00
---

From day to day, my job is to take care of my son, and will be for the
foreseeable future. While I enjoy him trotting up to me, book in hand,
speaking the unfathomable, I anticipate a break. When I get a break,
I like to spend it on something that unites all of my skills, interests,
and concerns, and lately that has been digital signal processing.

Though I spent the past year focusing on hardware, especially analog
circuits, it's not the best thing for me to focus on right now, for
many reasons. I love analog electronics, so this was a bittersweet
realization. But DSP makes me more content.

Even before I became a software developer, analog prototyping felt
inconvenient and awkward. Fishing around for components just to tweak
something, debugging by squinting at wires and probes; dare I say,
analog prototyping can be demanding of one's eyesight. You also need
specialized equipment, some of which can be hard to find. Many times
I've wanted a high-impedance audio probe---a way to poke any node in a
circuit to hear the signal---but never procured one. You can spend a lot
of money and time building a lab without getting any work done.

Analog prototyping can differ significantly from production. If you want
to be more than just a hobbyist, you ought to make prototypes using the
same technology you intend to use for production. This requires making
a tough decision up front, which increases pressure to get it right
the first time. PCB manufacture is pretty affordable for hobbyists,
but automated PCB assembly can cost thousands of dollars and is not
cost-effective for low volumes. When you're spending your own cash,
it is daunting.

If you want to design for robustness and repairability, you should
prefer through-hole construction. It kicks surface-mount technology's
ass in terms of longevity; that's the reason military and aerospace
applications use it. I also know this first-hand from all the mid-20th
century gear I've peered into. The boards were made with a highly
accessible technology.

But through-hole is bulky (→ costly) and suffers from a limited selection
of components. Just the other month, the through-hole package of the
LM13700 operational transconductance amplifier (OTA)---on which I was
basing a recent design---was EOL'd by Texas Instruments. Bam, my design
has just been invalidated. OK, I'll look into THAT Corporation ICs. If
it's good enough for Elektron Music Machines, it's probably good enough
for me.

But wait, why are we doing this? You love analog so much you're willing
to use expensive components offered by only one company? OK, I'll use
discrete transistors to build my own OTAs---no, you can't get good enough
matching for that. GAH. OK, transistor arrays? Good luck finding them.
Fuck it, just use pots; they'll have less noise.

Nevermind the troubles of the bill of materials. Hardware production is
problematic to any person who is concerned about e-waste. We already
overproduce electronics and throw them out for bad reasons (to make
money); so how are we going to justify producing more electronics? I
might say I have a uniquely analog design that can't be realized by any
other means. But I doubt the claim; can one prove that an idea cannot be
realized by mere software? (And how can one guarantee that the customer
isn't going to dump it in the landfill when it breaks?)

Mere software is quite attractive from the perspective of hardware reuse.
I can write platform-agnostic DSP code and run it on salvaged hardware,
no new production required. Still sounds awesome, assuming I did my job
right. Plus, it can be updated. Hard decisions can be postponed.

That feels familiar. DSP development is software development, something I
actually did for a living. I learned many powerful techniques to assist
me in that endeavor, many of which have no place in analog circuit
design. DSP can be developed with a rapid feedback loop, making it much
more fun to work on. Need to tweak something? Just edit the code, build
it, and listen.

DSP has challenges that analog doesn't, especially if you're modeling
nonlinear analog systems. This is far from simply applying a bilinear
transform; you need much more insight into the system being modeled. And
because you're choosing the model based on what might be useful for
musical expression, there is a lot of freedom in the design.

That makes it a bit fun, because many things can be tried. But it also
exposes you to a much larger mathematical universe, which means you're not
guaranteed to succeed. Modeling nonlinear dynamical systems is hard work,
and people who are good at it are [well compensated][bls]. Even they encounter
problems they cannot solve. There is a reason that accurately predicting
the weather two weeks from now is impossible: complexity.

[bls]: https://www.bls.gov/ooh/math/mathematicians-and-statisticians.htm#tab-5

You might have success over certain ranges of parameters, but not others,
and be left scratching your head; why does the model absolutely flip
shit when the cutoff frequency is too high? The intuition by which analog
circuits can be designed is not useful in such cases; it requires insight
into how nonlinear time-varying discrete-time dynamical systems work, and
what conditions are required for their stability. This means you have to
read thick textbooks, which you may shut with more questions than answers.

Sometimes the difficulty of nonlinear DSP gives me pause; maybe
_this_ is the reason to insist on analog hardware. But I don't think
that's a good answer.  I want to see the state of the art advance to
the point that analog hardware is never _necessary_ from an auditory
perspective. Arguably we have already attained this long ago. Yet somehow
a shred of doubt remains (which markets have noticed). Yes, I agree,
simulation generally has limitations. It wouldn't be sensible to require
a supercomputer to get an ear-accurate simulation of an analog circuit.

Fine then, technology always has limits---but part of the job of a
creative person is to exploit them for the sake of expression. Constraints
are important to creativity, so ultimately musicians should not be
disturbed by the fact that instruments made with DSP have limitations. The
constraints might _differ_ from those of other technologies, but this
just requires acclimation, not a technological miracle.

After all, that's exactly how analog became so beloved in the first place.
