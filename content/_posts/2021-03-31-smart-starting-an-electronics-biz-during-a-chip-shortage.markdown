---
author: kdsch
date: 2021-03-31 17:09:15+00:00
draft: false
title: 'Smart: starting an electronics biz during a chip shortage'
type: post
url: /2021/03/31/smart-starting-an-electronics-biz-during-a-chip-shortage/
---




At some point while designing an electronic product, one has to decide the hardware implementation. The number of options is a thing of awe. To gather relevant information and focus on one aspect at a time can get you past the paralysis. And make your requirements and constraints clear.







Should you face a shortage of semiconductors, you want to avoid committing to special chips. Aim for the least common denominator, seems right. I look at what's in stock now and might be relieved to find things that could work, yet fear them scarce come production time. Companies nimble enough are redesigning their products to use whatever is available. Others hoard. Wisdom seems to be that nobody should expect the shortage to clear up before '22.













By the way, I have a funny little book, _[People's Republic of Walmart](https://www.versobooks.com/books/2822-the-people-s-republic-of-walmart)_. I am sad it has no index, but it tells good stories to one-up capitalism. Walmart got sick of being whipsawed by a loose-knit supply chain. They stabilized it by assimilating it whole and enforcing transparency and coordination. The book tells this story to show that firms plan and cooperate inside. I tell it because bloips like this are avoidable, not just economic weather!







Glad I left the auto industry.













If I am to produce before October, I must weather the shortage. Of course, I don't need millions of units, but for some parts you're lucky to get 100.







My product's requirements might be eased. External memory narrows the choice a lot. Can I make a sampler that uses only an SD card? [Bastl did](https://bastl-instruments.com/instruments/microgranny), granted, with a quarter of the audio bitrate. I'll try it soon, guessing that a fast SPI peripheral helps.







The least common denominator is a resource fit for a company that wants to put value in the commons rather than suck it out. That's [Haxbli](http://haxb.li/), the little LLC I founded last month.



