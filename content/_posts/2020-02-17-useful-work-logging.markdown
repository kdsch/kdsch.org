---
author: kdsch
date: 2020-02-17 17:25:21+00:00
draft: false
title: Useful Work Logging
type: post
url: /2020/02/17/useful-work-logging/
tags:
- narrative
- process
- work logging
---

Ticket systems such as Atlassian Jira can bridge developers and product
owners, if used effectively. Developers don't need see logging their work
as an arbitrary chore that happens to please management. Work logging
keeps product owners informed about development pace and can help teams
of developers use resources efficiently.

I use the verbal phrase "work logging" rather than the noun phrase "work
log" to emphasize the practice over the artifact. The goal isn't merely
to produce an artifact, but to tell the story of development.

The one-liner ([excerpted from Jira's
help](https://confluence.atlassian.com/jirasoftwarecloud/logging-work-on-issues-902499028.html),
no less) gives us the quintessential example of a useless work log:

![](/Timetrackingfields.png)
How _not_ to log work, courtesy of Jira.

Developers might produce such artifacts for a variety of reasons, such as

- expecting that nobody will read it
- overworking
- trying to satisfy management
- finding it redundant
- inexperience

It doesn't help that Jira's work log form emphasizes time over
description, misleading newcomers to the practice. Referring to the
practice as "logging time" also misses the point.

Situations differ; useful work logs pitch themselves to the needs of
the situation. But typically, a one-liner does not suffice.

Anthropologists call humans "story-telling apes" for a good reason. We
distinguish ourselves from our evolutionary relatives mainly through
our use of narrative. Second-generation cognitive science also attests
to the centrality of narrative in cognitive structure. Storytelling is
the operating system of the human brain.

Thus, it should be no surprise that a useful work log **tells a
story**. In Jira, the ticket itself sets the opening of the story. Thus,
a poorly written ticket can inspire useless work logs.

Here's a possible improvement over the above one-liner:

```
Log time:         3h
Time remaining:   6h
Date started:     2019/02/06 8:42am

Work description: To determine the performance impact
of recent refactorings (see tickets #45, #97), I
tested the system against a range of load profiles.
Most of this time was spent learning to configure the
new load testing tool, Testy McLoader, which I found
counterintuitive. I found the following resources
helpful to this end: ... I am increasing the time
remaining to account for this.

I also checked a script into the repository that
helps to smooth some of TML's rough edges. We might
want to consider some alternatives (see #98, which I
just opened).

Anyway, here are the results of the test (which can
also be found in the commit log):

...

I expect to polish up our memory management scheme
next, which could make it easier to work in the
database calls.
```

How useful is this work log? Far more than enumerating what was done,

- **It places the work in context.** A desire to understand the
  performance impact of refactorings motivated the load testing.
- **It reveals details about resource use.** Configuration took more
  time than expected.
- **It informs the team of obstacles encountered** and resources by
  which they might be overcome.
- **It sketches future expectations**, noting their uncertainty.

_Aside:_ A colleague recently asked how I deal with missed deadlines. A
missed deadline often results from poor communication, leading to a
divergence between stakeholder expectations and reality. The task isn't
what to do _after_ a deadline is missed (you might as well enjoy a glass
of wine), but how to _maintain_ realistic deadlines. Useful work logging
can do just that.
