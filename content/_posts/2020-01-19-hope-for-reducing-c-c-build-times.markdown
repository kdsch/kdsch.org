---
author: kdsch
date: 2020-01-19 17:40:33+00:00
draft: false
title: Hope for Reducing C/C++ Build Times
type: post
url: /2020/01/19/hope-for-reducing-c-c-build-times/
tags:
- automation
- build tools
- migration
- optimization
---




When I write software, I'm often motivated by a desire for structural simplicity. My low tolerance for complexity makes me avoid certain solutions, even if it means more work. A love of simplicity, in some contexts, is a strength. In others, it's a weakness.







Embedded Linux development often entails building large amounts of system software, which is typically written in C and C++. Build times on the order of hours limit the speed of the development cycle. Many have complained about this situation, but feel little hope for improvement. 







Similar motivations led to the creation of the Go programming language, which decided to break compatibility with the C/C++ ecosystem in order to revise a several of their design choices. Go reduced build times by orders of magnitude through a holistic approach; [several choices](https://talks.golang.org/2012/splash.article) taken together created this improvement.







C and C++ have not been designed to quickly compile large amounts of code, and the constraint of backward compatibility leaves little hope for the radical changes that would improve this metric.







A project by Andy Chu demonstrates a more gradual approach to revising language design decisions. [Oil shell](http://www.oilshell.org/blog/2018/01/28.html) provides a transpiler that translates a "sane" subset of shell (called OSH) into an incompatible shell language (Oil). The goal is not to improve performance, but to simplify the language.







There are significant obstacles to reducing C/C++ compilation times by several orders of magnitude, many of which cannot be anticipated. Nevertheless, the rise of new build systems such as [Meson](https://mesonbuild.com/) demonstrate an interest of developers in better tooling. I'm encouraged to see Meson's main contributor, Jussi Pakkanen, [thinking productively](https://nibblestew.blogspot.com/) about this situation.







One issue with C/C++ build systems is that they typically invoke multiple new compiler processes to build a single artifact. The compiler knows low-level details, yet the build system understands the overall project structure, which makes it difficult to leverage all of this information at one time.







[C2Rust](https://c2rust.com/) is a transpiler that converts C to Rust. However, Rust is not known for quick compilation either.







I'm not aware of many choices for C/C++ developers and embedded Linux system integrators who want to speed up their workflow. If you know of any, please [let me know](https://schultheisz.com/contact/).



