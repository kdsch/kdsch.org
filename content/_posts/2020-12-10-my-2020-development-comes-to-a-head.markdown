---
author: kdsch
date: 2020-12-10 12:44:14+00:00
draft: false
title: 'My 2020: Development Comes to a Head'
type: post
url: /2020/12/10/my-2020-development-comes-to-a-head/
categories:
- Personal
---




Over the past 15 months, my life has rapidly transformed. But for you to understand the significance of that transformation, I have to tell you the story of my life since high school.







In 2001, I was fourteen years old, skateboarding, and listening to the Chemical Brothers. When I entered tenth grade, I enrolled in an electronic  technology program at a [vo-tech school](https://www.berkscareer.com/), a decision to exert greater control over my education, not to skip college and become a technician. Leonard Krug, my instructor, recognized and nourished my curiosity with significant latitude to explore. Voltage-controlled filters enchanted me, and I built a four-pole filter using LM13600's as part of a joint senior project. (The other part was a digital oscillator/sequencer designed by Andrew Meehan, my friend to this day.) During this period, I received a book that offered permanent career inspiration: _The Art and Science of Analog Circuit Design_, by Jim Williams.







After high school, I intended to study electrical engineering. My first semester at Penn State Berks, however, convinced me to drop out. The rigid system of prerequisites, in particular, was a step back from the nurturance I received at vo-tech. For the next three years, I tried to find my place. I thought maybe audio engineering school would be worth a shot, and enrolled at the Art Institute of Seattle in 2008. I did better, dropping out after a year and half. In 2010, I realized that compared to everyone else I knew, I was an electrical engineer at heart, and I saw no hope in making a career of it without a degree. So I went back with more resolve than ever.







While attending Penn State at the main campus, I began to think that my love of analog audio electronics was a liability. I again locked horns with the prerequisite system, and in my spare time explored and nourished new personal interests, such as Linux, Lisp, and philosophy. I graduated. A desperate year-long job search convinced me that my analog muse was dead. It was 2016, not 1979.







I finally took a job at ESG Automotive in Troy, Michigan, moving 600 miles to work on a project at Ford Motor Company. We were to develop a so-called "remote" radio tuner module, in the sense that it is not integrated in the audio head unit. I marveled at how difficult it was to be productive in such a large, complex organization. We worked at such a high and abstract level, with such bureaucracy, that I found it nearly impossible to see any engineering in my work.







My involvement came to an abrupt end after my Ford boss made a mistake with his next ESG purchase order. He wasn't able to secure enough funding, so I was told, and one ESG person had to be cut from the team. I offered to go, as I felt that I could find a better fit elsewhere within ESG. I was given a rare in-house embedded Linux role. I began working with a small team of software engineers, then remotely, and eventually moved back to Pennsylvania. I had regained some autonomy; my curiosity had been vindicated!







In September 2019, I lost my full-time job at ESG Automotive. This was difficult timing, as my wife Steph and I were expecting our first kid in April. I began a painstaking and discouraging job search. After three months of scavenging job boards, interviewing, and doubting that I would be hired, I started networking, which many experts tout as the best way to get a job, or to do anything, really. I believe it. Most people who encounter success depend on their connections to enablers. So around January 2020, I began looking for people to connect with and learn from.







Two friendly software consultants, each with an extensive online profile, were instrumental to me deciding to go into the business. [Jason Mundok](https://www.jasonmundok.com/), who works in my city with small businesses, let me interview him for two hours. After a phone call with [Cliff Brake](http://bec-systems.com/site/about), an embedded Linux developer in Ohio, he became my first customer as I worked on projects for his clients. While I had foregone the format of a typical job, I felt it was a good trade-off. Some family members worried that independent consulting lacked stability, but having been fired, I didn't see stability as a feature of any kind of employment. I had gained some autonomy and humanity. (And I enjoyed learning frontend web development---[without touching JavaScript](https://elm-lang.org/)!)







My son Otto was born in April, just as the first wave of the pandemic crested. I took a break from my work with Cliff to become a parent. During the first three or four months, the entire family wrestled with health problems. Otto eventually developed the ability to sleep through the night in his own crib in his room. This was an important shift, but it would not be the end of my difficulties. Just recently, I have realized how hard it is for me to handle Otto’s developmental shifts. I may be reaching a stage of greater acceptance and understanding of him.







As Otto became more independent, I found time to return to activities I had enjoyed years ago but abandoned. I began to explore digital signal processing (using [Faust](https://faust.grame.fr/)) with rigorous mathematics. Then I started getting ideas for musical instruments, like wearable electronic percussion, and an instrument inspired by [late 1990's drum & bass production workflows](https://www.reddit.com/r/edmproduction/comments/1vagak/i_am_optical_ama_virus_recordings/ceqbl25/?context=3). I started thinking about voltage-controlled filters, distortion, analog modelling, and PCB design. And then about the user experience of musical instruments.







As I explored these ideas, I felt that my teenage dream had come within reach. I realized that I am thirty-three years old, and not anybody, even I, can tell me that I can't do this. I can't tell you how good this feels after almost two decades of trying to find my place in society. It's something I'm so passionate about, I'll go into business to make it happen.







Stay tuned.



