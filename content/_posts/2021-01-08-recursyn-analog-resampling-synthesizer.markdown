---
author: kdsch
date: 2021-01-08 05:06:10+00:00
draft: false
title: 'Recursyn: analog resampling synthesizer'
type: post
url: /2021/01/08/recursyn-analog-resampling-synthesizer/
categories:
- Personal
---




I've been listening to drum & bass music since my teens, but only late last year did I study in detail the history of its production. Drum & bass was the among the first music genres to explore the remotest sound design possibilities of digital sampling. This exploration resembled an unwitting turn of electronic music toward a much earlier musical movement, _musique concrète_, which employed similar techniques of unrecognizable sound. The golden age of drum & bass coincided with the apex of hardware samplers, which were rapidly killed off by personal computers running digital audio workstations. Drum & bass survived, but in symbiosis with different tools.







I began making music young enough and long enough after the demise of this hardware to have had no idea what I had been listening to. My first digital music tools were Sonic Foundry Acid 3 and Fruity Loops 3. My first hardware was a Novation K-Station. Gear such as the Emu e6400 Ultra, with a user manual of thousands of pages, were plainly out of my reach as a teenager, and, I imagine would have been much too difficult to use.







Pick a year in the late 90s, and the otherworldly sounds you hear on drum & bass records are likely produced not just by sampling old analog monosynths or sci-fi films or found objects, but by resampling them over and over through outboard gear. And true to the nature of musical innovation, that outboard processing was being repurposed and pushed outside its design envelope. The artist Optical, for instance, was in the habit of overdriving anything with a gain control, and settled on a particular studio equalizer as his favorite.







Having learned this, I lamented not being able to experience it myself. Not for nostalgia, but the creative experience. Today's in-the-box signal processing, while powerful, proceeds along the axes of plugin chains. Resampling, however, offers a different set of possibilities, if cumbersome. By recording each of the intermediate signals, new processing possibilities are created. It isn't just a desire to re-live the sounds, but the _workflow_. Yet the samplers of old are still beyond my budget and my patience.







But wait. I'm an engineer. I can do it myself.







I can do it _better_.







And with that impulse, Recursyn was conceived.







Recursyn is the codename of an instrument that treats resampling as a synthesis technique. The core of its architecture is a digital sampler connected in a feedback loop with an analog signal processing chain. It could be all digital, yes. I am no analog purist; I confess to having had as much fun with operational transconductance amplifiers as I have with hyperbolic tangent functions. The reason for it to be analog is: because I said so.







Other rationalizations are possible. Analog circuits give you nonlinearities for free. Distortion and chaos for free. No need for complex mathematical analysis to make DSP act that way. The virtue of analog computation is precisely the property of being analogous.







Recursyn aims to streamline the user experience of resampling as much as possible. Any parameter critical to sound design ought to have a dedicated front-panel control. This includes controls that affect the behavior of the sampler as a recording device. In normal use, there should be no need to hunt through menus.







Recursyn, as a proof-of-concept, focuses on monophonic sound design by means of resampling through analog processing. It sacrifices other functionality such as sequencing, full programmability, and other gimmies. We have to prove that resampling is fun and unique, not try to beat Elektron and Korg at their own games. Still, the instrument should be able to function in the context of a music production setup, so will need some means of interfacing with other tools. MIDI, CV, and USB are likely.







So far I haven't given nearly enough detail as would be required by a specification. And that's because the instrument is mostly still a dream. Only part of it has been prototyped at the time of writing. There's so much more to be discovered and realized. I long to see this vision brought to the world, but before that, we must arrive at a full prototype. And that requires a design, which requires knowing the underlying technologies.







I've never created a sampler before. Nevertheless, eventually I decided that the Microchip ATSAMD51, for its large number of IOs, FPU support, and decent open source presence, is a good place to start. The FPU will enable Faust as the DSP language, allowing me to near my life goal of never writing another line of C/C++. The IOs will come in handy for interfacing with the UI and analog processing engine. And hopefully we won't need much else aside from a memory chip, some connectors, and a handful of analog ICs. And a PCB. And pots and switches. And an enclosure.







And a bit of [sweat](https://git.sr.ht/~kdsch/recursyn).



