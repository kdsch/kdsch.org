---
author: kdsch
date: 2021-01-24 22:11:00+00:00
draft: false
title: Recursyn development report 0
type: post
url: /2021/01/24/recursyn-development-report-0/
categories:
- Musical Instrument Development
---




What's the current status of Recursyn?







I'm evaluating developing the firmware in Rust, particularly the application logic and peripheral drivers. Rust is attractive because it is a modern language, simpler than C++, targeting microcontrollers, with a large enough community to provide most of the functionality Recursyn needs. Rust comes with challenges, however; the language reference is incomplete at 316 pages, and its ecosystem is known to often break APIs. In addition, [typestate interfaces](https://docs.rust-embedded.org/book/static-guarantees/typestate-programming.html), which are often used in embedded Rust to enforce contracts, seem to incur a cost in readability. If Recursyn ultimately suffers from these tradeoffs, it probably needs C.







I've created some basic [firmware](https://github.com/kdsch/recursyn/tree/main/fw) demonstrating the functionality provided by the [atsamd-rs](https://github.com/atsamd-rs/atsamd) and [Real-Time Interrupt-driven Concurrency (RTIC)](https://rtic.rs/0.5/book/en/) projects. The board-support packages (BSPs) provided by atsamd-rs do not expose the full functionality of the hardware. The maintainers are open to contributions that would give users more control. As Recursyn uses these BSPs during the prototyping phase, I might contribute such enhancements to atsamd-rs.







I've chosen the [Adafruit Grand Central M4](https://www.adafruit.com/product/4064) (GCM4) development board. While it has been designed for some degree of Arduino compatibility, this is irrelevant now that I have chosen to try Rust. Given that Arduino compatibility isn't important, I could move to a different microcontroller in the future, perhaps from the STM32 line, which seems popular in Rust, but I haven't looked closely at community support.







Faust remains a strong candidate for developing DSP algorithms, as it provides a very concise syntax and compiles to Rust. The Rust crate that Faust generates could be improved to make better use of Rust's type system and to move logic from run time to compile time. Assuming the project sticks with Rust, I might contribute these improvements to the Faust compiler.







The analog processing engine will contain an aggressive sound-shaping tool: a two-pole state-variable filter (SVF) providing lowpass, bandpass, and highpass transfer functions. Its prototype, implemented with two LM13700 ICs, inspires me every time I play with it. When it is overdriven with high resonance, a warbling effect is achieved. The SVF has the advantage that, when it is dropped into a constant-Q equalizer topology, the circuit can provide six different transfer functions from a single filter core.







There remains uncertainty as to whether one microcontroller can do everything Recursyn needs: responding to the UI, streaming audio through the DSP engine, and emitting control signals. However, Rust's strengths in concurrency might help us to get the most out of one microcontroller without the usual risk of undefined behavior.







One possibility for prototyping the whole system is to create a custom shield for the GCM4. To reduce costs and ease fabrication, I would probably have to eliminate all SMT components, which could require the shield to have much larger dimensions than the development board. In that case, it would make sense to move the headers on the GCM4 to the bottom side so that the shield mounts beneath. In any case, I've never designed a shield before, so I'll learn a lot.



