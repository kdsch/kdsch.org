---
author: kdsch
date: 2020-09-22 16:37:10+00:00
draft: false
title: The Musician Taught the Engineer
type: post
url: /2020/09/22/the-musician-taught-the-engineer/
categories:
- Collaboration
---




Tatsuya Takahashi, a designer of modern analog synthesizers, has a coherent philosophy of musical instrument design. He argues that the guitar has been a dominant instrument because of its superiority in a number of aspects. The guitar is versatile: it can play chords or leads, rhythms or melodies. The guitar is portable: light enough to carry, loud enough for a small gathering. The guitar is autonomous: it is self-powered and needs no special environment, such as a production studio, to make music. The guitar is expressive: it responds inherently to nuances of touch and articulation, which a skilled performer can put to musical use. The guitar is simple: a set of strings attached to a wooden resonator, not banks of knobs and switches.







Historically, synthesizers have been inferior in most of these aspects. Given their focus on signal processing, they have compromised on versatility, portability, autonomy, expressiveness, and simplicity. Tatsuya's challenge is to beat the guitar with electronics. One consequence of this thinking is that many of his designs are battery-powered and include a built-in speaker.







I think we can go further, especially by using more tactile and intuitive interfaces. A guitar has very few variable parameters: the strings, the plectrum, the tuning. Aside from these, all variations in sound are achieved through performance, not configuration. The more of the instrument is playable, the more musicians can explore different playing techniques.













Musicians often take technique in unconventional directions. Weather Report's "Milky Way" does not sound like the "duet for saxophone and acoustic piano" as which it is listed. More specifically, it is a _musique concrète_ composition of chords sounded by the saxophone playing arpeggios directly into the soundboard of the piano for sympathetic resonance. The resonances, recorded to tape, are then spliced together.







While such phenomena as sympathetic resonance, noise, and distortion might be regarded by engineers as objectively undesirable, musicians have still taken advantage of them to varying degrees. While engineering provides the technology with which music is made, it does not provide technique. The piano manufacturer would not have marketed its products for sympathetic resonance purposes; at least, not until the commercial success of a composition such as "Milky Way". It didn't happen. Otherwise, maybe pianos would be sold with built-in saxophones.







Musical exploitations of engineering "flaws" have panned out in different ways. Who could have predicted that overdriven guitar amplifiers would have become a thing? In the early days, the amplifier manufacturers did their best to pass off their crude circuits as linear. Creating a linear amplifier of sufficient power would have been cost-prohibitive. In any case, the amp manufacturers quickly took advantage the new techniques electric guitarists were using. The musician taught the engineer how to make an instrument.













Being both a musician and an engineer, I see instrument design from both sides. No musician wants to feel themselves limited only to the palette of possibilities conceived during the design phase. No engineer wants to lose sight of the context in which their design performs a function. The fact is that engineers provide structures, whereas musicians determine functions. The two have to collaborate.



