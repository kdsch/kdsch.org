---
author: kdsch
date: 2021-01-04 16:42:17+00:00
draft: false
title: Faust Brings Functional Programming to DSP
type: post
url: /2021/01/04/faust-brings-functional-programming-to-dsp/
categories:
- Programming
---




>In the future, all radical new languages will be domain-specific languages.
> 
> [Peter Alvaro](https://www.infoq.com/presentations/language-design-process/?itm_source=presentations_about_qcon-sanfrancisco-2018&itm_medium=link&itm_campaign=qcon-sanfrancisco-2018)







If you write digital signal processing code and you haven't encountered [Faust](https://faust.grame.fr/), it's worth a look, especially if you have ever wanted a change of pace from writing DSP in C/C++. Faust is a functional language for DSP that cuts through the noise.







I first used functional programming professionally when last year I contributed to a web GUI in [Elm](https://elm-lang.org/). Many of the arguments for using Elm over JavaScript are also relevant to using Faust over C/C++. Elm is simple enough that I came to grasp it in a few months. I was hooked; I started using it for hobby projects, like a [CIEXYZ-to-RGB color picker](https://ellie-app.com/bZ5PMqncjVQa1). And when I started using Faust a few months ago, I felt the same zing.







In terms of problem domain, Faust has little in common with Elm. But the two languages have very similar feel in both syntax and semantics. The high level of these languages allows them to be compiled to many different target platforms. Faust can run in browsers just as well as microcontrollers such as the Microchip SAMD51. (It works best on systems that have a Floating-Point Unit.) And because it compiles to C++, it can be integrated with non-DSP code.







I'm not sure to what extent the functional programming craze has penetrated the embedded systems community (my guess: not much) but Faust might well contribute to the spread there, as it provides an alternative to C++ that still integrates with C++ codebases. In my view, functional programming improves code quality by reducing irrelevant details in codebases, emphasizing the stuff that matters. This argument is pretty rigorous in situations where the problem domain is well-defined, like user interfaces and signal processors. I admit that it is weaker when the problem has lots of cross-cutting concerns.







I'll be strongly considering Faust for the DSP implementation of Recursyn, a musical instrument I am developing, which I'll probably write about soon.



